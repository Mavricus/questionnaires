create table questionnaires
(
	id text
		constraint questionnaires_pk
			primary key,
	description text
);

create table questions
(
	id text not null
		constraint questions_pk
			primary key,
	text TEXT not null,
	questionnaire_id text not null
		references questionnaires
			on update cascade on delete cascade,
	is_initial INTEGER default 0
);

create table answers
(
	id TEXT not null
		constraint answers_pk
			primary key,
	text TEXT not null,
	question_id text not null
		references questions
			on update cascade on delete cascade
);

create index answers_question_id_index
	on answers (question_id);

create table next_questions
(
	answer_id text
		references answers
			on update cascade on delete cascade,
	question_id text
		references questions
			on update cascade on delete cascade,
	constraint next_questions_pk
		primary key (question_id, answer_id)
);

create index questions_questionnaire_id_index
	on questions (questionnaire_id);

