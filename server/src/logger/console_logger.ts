export interface ILogMessage {
    description: string;
    requestId: string;
    err?: Error;
    error?: Error;
}

export interface ILogger {
    trace<T extends ILogMessage>(message: T);
    debug<T extends ILogMessage>(message: T);
    info<T extends ILogMessage>(message: T);
    warn<T extends ILogMessage>(message: T);
    error<T extends ILogMessage>(message: T);
    emergency<T extends ILogMessage>(message: T);
    setLoglevel(level: string);
}

export enum LogLevels {
    trace = 1,
    debug = 2,
    info = 3,
    warn = 4,
    error = 5,
    emergency = 6,
}

interface IConsole {
    log(message?: any, ...optionalParams: any[]): void;
    error(message?: any, ...optionalParams: any[]): void;
}

export class ConsoleLogger implements ILogger {
    constructor(private console: IConsole) {
    }

    private logLevel = LogLevels.debug;

    debug<T extends ILogMessage>(message: T) {
        this.logMessage(LogLevels.debug, message);
    }

    emergency<T extends ILogMessage>(message: T) {
        this.logMessage(LogLevels.emergency, message);
    }

    error<T extends ILogMessage>(message: T) {
        this.logMessage(LogLevels.error, message);
    }

    info<T extends ILogMessage>(message: T) {
        this.logMessage(LogLevels.info, message);
    }

    trace<T extends ILogMessage>(message: T) {
        this.logMessage(LogLevels.trace, message);
    }

    warn<T extends ILogMessage>(message: T) {
        this.logMessage(LogLevels.warn, message);
    }

    setLoglevel(level: string) {
        this.logLevel = LogLevels[level];
    }

    private logMessage<T extends ILogMessage>(logLevel: LogLevels, message: T) {
        if (logLevel < this.logLevel) {
            return;
        }

        let text = this.buildMessage(logLevel, message);

        if (logLevel < LogLevels.error) {
            this.console.log(text);
        } else {
            this.console.error(text);
        }
    }

    private buildMessage<T extends ILogMessage>(logLevel: LogLevels, message: T): string {
        let scope = {
            dateTime: new Date().toISOString(),
            loglevel: LogLevels[logLevel],
            data: Object.assign(message),
            requestId: message.requestId
        };
        delete scope.data.requestId;

        if (scope.data.err != null && scope.data.err instanceof Error) {
            scope.data.err = scope.data.err.stack;
        }

        if (scope.data.error != null && scope.data.error instanceof Error) {
            scope.data.error = scope.data.error.stack;
        }

        return JSON.stringify(scope);
    }
}