import { IQuestion, QuestionnairesProvider } from './questionnaires_provider';
import { LoggerStub } from '../../stubs/logger_stub';
import { QuestionsDaoStub } from '../../stubs/dao/questions_dao_stub';
import { AnswersDaoStub } from '../../stubs/dao/answer_dao_stub';
import { NextQuestionsProviderStub } from '../../stubs/dao/next_question_strategy_stub';
import * as sinon from 'sinon';
import { IAnswerData } from './sql/answer_dao';
import { IQuestionData } from './sql/questions_dao';
import { QuestionnairesDaoStub } from '../../stubs/dao/questionnaires_dao_stub';

describe('QuestionnairesProvider', () => {
    let provider: QuestionnairesProvider;
    let logger: LoggerStub;
    let qDao: QuestionsDaoStub;
    let aDao: AnswersDaoStub;
    let nqProvider: NextQuestionsProviderStub;
    let questionnairesDao: QuestionnairesDaoStub;
    const requestId = 'Test request';
    const answersByQuestionId: Array<IAnswerData> = [{ id: '1', text: 'Yes', questionId: '1' }, { id: '2', text: 'No', questionId: '1' }];
    const questionData: IQuestionData = {
        id: '111',
        text: 'Do you test me?',
        questionnaireId: '1',
        isInitial: true
    };

    beforeEach(() => {
        logger = new LoggerStub(sinon);
        logger.defineDefaultBehaviour();

        qDao = new QuestionsDaoStub(sinon);
        qDao.stubs.createQuestion.resolves();
        qDao.stubs.deleteQuestion.resolves();
        qDao.stubs.deleteQuestions.resolves();
        qDao.stubs.getQuestionById.resolves(questionData);
        qDao.stubs.getQuestionsById.resolves([questionData]);
        qDao.stubs.updateQuestion.resolves();

        aDao = new AnswersDaoStub(sinon);
        aDao.stubs.createAnswer.resolves();
        aDao.stubs.deleteAnswer.resolves();
        aDao.stubs.deleteAnswers.resolves();
        aDao.stubs.getAnswersById.resolves(answersByQuestionId[0]);
        aDao.stubs.getAnswersByQuestionId.resolves(answersByQuestionId);
        aDao.stubs.updateAnswer.resolves();

        nqProvider = new NextQuestionsProviderStub(sinon);
        nqProvider.stubs.get.resolves();

        questionnairesDao = new QuestionnairesDaoStub(sinon);
        questionnairesDao.stubs.getAllQuestionnaires.resolves([
            { id: 1, description: 'Test 1' },
            { id: 3, description: 'Test 2' }
        ]);

        provider = new QuestionnairesProvider(logger, qDao, aDao, nqProvider, questionnairesDao);
    });

    describe('createQuestion', () => {
        let question: IQuestion = {
            id: '2123',
            text: 'What is love?',
            questionnaireId: '1',
            isInitial: true,
            answers: [
                { id: '1', text: 'I do not know', questionId: '1' },
                { id: '2', text: 'Nobody knows', questionId: '1' }
            ]
        };
        
        it('returns newly created question data', async () => {
            await expect(provider.createQuestion(requestId, question)).resolves.toBeUndefined();
        });

        it('rejects with the same error that Question DAO rejected', async () => {
            let exception = new Error('Test exception');
            qDao.stubs.createQuestion.rejects(exception);
            
            await expect(provider.createQuestion(requestId, question)).rejects.toBe(exception);
        });

        it('rejects with the same error that Answer DAO rejected at least one time', async () => {
            let exception = new Error('Test exception');
            aDao.stubs.createAnswer.onFirstCall().rejects(exception);

            await expect(provider.createQuestion(requestId, question)).rejects.toBe(exception);
        });
    });

    describe('deleteQuestion', () => {
        let id = '123';

        it('returns true if question was successfully created', async () => {
            await expect(provider.deleteQuestion(requestId, id)).resolves.toBeUndefined();
        });

        it('rejects with the same error that Question DAO rejected', async () => {
            let exception = new Error('Test exception');
            qDao.stubs.deleteQuestion.rejects(exception);

            await expect(provider.deleteQuestion(requestId, id)).rejects.toBe(exception);
        });

        it('rejects with the same error that answers DAO rejected on answers by question ID', async () => {
            let exception = new Error('Test exception');
            aDao.stubs.getAnswersByQuestionId.rejects(exception);

            await expect(provider.deleteQuestion(requestId, id)).rejects.toBe(exception);
        });

        it('rejects with the same error that answer DAO rejected', async () => {
            let exception = new Error('Test exception');
            aDao.stubs.deleteAnswers.rejects(exception);

            await expect(provider.deleteQuestion(requestId, id)).rejects.toBe(exception);
        });
    });
});