import { IQuestionData, IQuestionsDao } from './sql/questions_dao';
import { IAnswerData, IAnswersDao } from './sql/answer_dao';
import { ILogger } from '../logger/console_logger';
import { INextQuestionData, INextQuestionsProvider } from './sql/next_question_provider';
import { IQuestionnaireData, IQuestionnairesDao } from './sql/questionnaires_dao';
import uuid = require('uuid');

export interface IQuestion extends IQuestionData {
    answers: Array<IAnswerData>
}

export interface IQuestionnaire extends IQuestionnaireData {
    questions: Array<IQuestionData>;
}

export interface IAnswer extends IAnswerData {
    nextQuestions: Array<IQuestionData>;
}

export interface IQuestionnairesProvider {
    createQuestion(requestId: string, question: IQuestionData): Promise<IQuestionData>;
    getQuestionDetails(requestId: string, id: string): Promise<IQuestion>;
    getQuestions(requestId: string, ids: Array<string>): Promise<Array<IQuestion>>;
    getQuestionsByQuestionnaireId(requestId: string, id: string): Promise<Array<IQuestionData>>;
    deleteQuestions(requestId: string, ids: Array<string>): Promise<void>;
    deleteQuestion(requestId: string, id: string): Promise<void>;
    updateQuestion(requestId: string, question: IQuestionData): Promise<void>;

    getNextQuestions(requestId: string, answers: Array<string>): Promise<Array<IQuestion>>;
    deleteNextQuestion(requestId: string, data: INextQuestionData): Promise<void>;
    createNextQuestion(requestId: string, data: INextQuestionData): Promise<INextQuestionData>;

    getAllQuestionnaires(requestId: string): Promise<Array<IQuestionnaireData>>;
    getQuestionnaireDetails(requestId: string, id: string): Promise<IQuestionnaire>;
    getQuestionnaire(requestId: string, id: string): Promise<IQuestionnaireData>;
    deleteQuestionnaire(requestId: string, id: string): Promise<void>;
    createQuestionnaire(requestId: string, questionnaire: IQuestionnaireData): Promise<IQuestionnaireData>;
    updateQuestionnaire(requestId: string, questionnaire: IQuestionnaireData): Promise<void>;

    getAnswerById(requestId: string, answerId: string): Promise<IAnswer>;
    createAnswer(requestId: string, answer: IAnswerData): Promise<IAnswer>;
    deleteAnswer(requestId: string, answerId: string): Promise<void>;
    updateAnswer(requestId: string, answer: IAnswerData): any;
}

export class QuestionnairesProvider implements IQuestionnairesProvider {
    constructor(private logger: ILogger,
                private questionDao: IQuestionsDao,
                private answersDao: IAnswersDao,
                private nextQuestionProvider: INextQuestionsProvider,
                private questionnairesDao: IQuestionnairesDao) {
    }

    async deleteNextQuestion(requestId: string, data: INextQuestionData): Promise<void> {
        return this.nextQuestionProvider.delete(requestId, data);
    }
    
    async createNextQuestion(requestId: string, data: INextQuestionData): Promise<INextQuestionData> {
        return this.nextQuestionProvider.create(requestId, data);
    }

    async getAnswerById(requestId: string, answerId: string): Promise<IAnswer> {
        try {
            return Promise.all([
                this.answersDao.getAnswerById(requestId, answerId),
                this.nextQuestionProvider.get(requestId, [answerId])
                    .then(ids => this.questionDao.getQuestionsById(requestId, ids))
            ]).then(([answer, nextQuestions]) => QuestionnairesProvider.buildAnswer(answer, nextQuestions));
        } catch (err) {
            this.logger.warn({ description: 'Error occurred during getting answer information', err, requestId });
            throw err;
        }
    }

    async createAnswer(requestId: string, answer: IAnswerData): Promise<IAnswer> {
        try {
            return this.answersDao.createAnswer(requestId, answer)
                       .then(a => QuestionnairesProvider.buildAnswer(a, []));
        } catch (err) {
            this.logger.warn({ requestId, description: 'Error occurred during answer creation', err, answer });
            throw err;
        }
    }

    async deleteAnswer(requestId: string, answerId: string): Promise<void> {
        try {
            return Promise.all([
                this.answersDao.deleteAnswer(requestId, answerId),
                this.nextQuestionProvider.deleteByAnswerId(requestId, answerId)
            ]).then(() => null);
        } catch (err) {
            this.logger.warn({ requestId, description: 'Error occurred during answer deleting', err, answerId });
            throw err;
        }
    }

    async updateAnswer(requestId: string, answer: IAnswerData) {
        try {
            return this.answersDao.updateAnswer(requestId, answer);
        } catch (err) {
            this.logger.warn({ requestId, description: 'Error occurred during answer updating', err, answer });
            throw err;
        }
    }

    async getAllQuestionnaires(requestId: string): Promise<Array<IQuestionnaireData>> {
        try {
            return this.questionnairesDao.getAllQuestionnaires(requestId);
        } catch (err) {
            this.logger.warn({ description: 'Error occurred during getting all question', err, requestId });
            throw err;
        }
    }

    async getQuestionsByQuestionnaireId(requestId: string, id: string): Promise<Array<IQuestionData>> {
        try {
            let qs = await this.questionDao.getQuestionsByQuestionnaireId(requestId, id);
            let tasks = qs.map(q => this.getQuestionDetails(requestId, q.id));
            return await Promise.all(tasks);
        } catch (err) {
            this.logger.warn({ requestId, description: 'Failed to get questions by questionnaire ID', err, id });
            throw err;
        }
    }

    async createQuestion(requestId: string, question: IQuestionData): Promise<IQuestionData> {
        return this.questionDao.createQuestion(requestId, question);
    }

    async deleteQuestion(requestId: string, id: string): Promise<void> {
        try {
            return Promise.all([
                this.questionDao.deleteQuestion(requestId, id),
                this.answersDao.deleteAnswersByQuestionId(requestId, id),
                this.nextQuestionProvider.deleteByQuestionId(requestId, id)
            ]).then();
        } catch (err) {
            this.logger.warn({ description: 'Error occurred during question deleting', err, requestId });
            throw err;
        }
    }

    async deleteQuestions(requestId: string, ids: Array<string>): Promise<void> {
        try {
            return Promise.all([
                this.questionDao.deleteQuestions(requestId, ids),
                Promise.all(ids.map(id => this.answersDao.deleteAnswersByQuestionId(requestId, id)))
            ]).then();
        } catch (err) {
            this.logger.warn({ requestId, description: 'Failed to delete answers', ids });
            throw err;
        }
    }

    async getNextQuestions(requestId: string, answers: Array<string>): Promise<Array<IQuestion>> {
        return this.nextQuestionProvider.get(requestId, answers)
                   .then(ids => this.getQuestions(requestId, ids));
    }

    async getQuestionDetails(requestId: string, id: string): Promise<IQuestion> {
        try {
            return Promise.all([
                this.questionDao.getQuestionById(requestId, id),
                this.answersDao.getAnswersByQuestionId(requestId, id)
            ]).then(([question, answers]) => QuestionnairesProvider.buildQuestion(question, answers));
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot get question', id, err });
            throw err;
        }
    }

    async getQuestions(requestId: string, ids: Array<string>): Promise<Array<IQuestion>> {
        try {
            let tasks = ids.map(id => this.getQuestionDetails(requestId, id));
            return Promise.all(tasks);
        } catch (err) {
            this.logger.warn({ requestId, description: 'Cannot get questions', ids, err });
            throw err;
        }
    }

    async updateQuestion(requestId: string, question: IQuestionData): Promise<void> {
        return  this.questionDao.updateQuestion(requestId, question);
    }

    async getQuestionnaireDetails(requestId: string, id: string): Promise<IQuestionnaire> {
        try {
            return Promise.all([
                this.questionnairesDao.getQuestionnaireById(requestId, id),
                this.questionDao.getQuestionsByQuestionnaireId(requestId, id)
            ]).then(data => {
                let questionnaire: IQuestionnaireData = data[0];
                let questions: Array<IQuestionData> = data[1];
                return QuestionnairesProvider.buildQuestionnaire(questionnaire, questions);
            })
        } catch (err) {
            this.logger.warn({ requestId, description: 'Failed to delete answers', id, err });
            throw err;
        }
    }

    async getQuestionnaire(requestId: string, id: string): Promise<IQuestionnaireData> {
        return this.questionnairesDao.getQuestionnaireById(requestId, id);
    }

    async createQuestionnaire(requestId: string, questionnaire: IQuestionnaireData): Promise<IQuestionnaireData> {
        questionnaire.id = questionnaire.id ?? uuid.v1();
        return this.questionnairesDao.createQuestionnaire(requestId, questionnaire).then(() => questionnaire);
    }

    async deleteQuestionnaire(requestId: string, id: string): Promise<void> {
        return Promise.all([
            this.questionnairesDao.deleteQuestionnaire(requestId, id),
            this.deleteQuestionsByQuestionnaireId(requestId, id)
        ]).then();
    }

    async updateQuestionnaire(requestId: string, questionnaire: IQuestionnaireData): Promise<void> {
        return this.questionnairesDao.updateQuestionnaire(requestId, questionnaire);
    }

    private async deleteAnswers(requestId: string, ids: Array<string>): Promise<void> {
        return this.answersDao.deleteAnswers(requestId, ids);
    }

    private static buildQuestion(question: IQuestionData, answers: Array<IAnswerData>): IQuestion {
        return Object.assign({ }, question, { answers });
    }

    private static buildQuestionnaire(questionnaire: IQuestionnaireData, questions: Array<IQuestionData>): IQuestionnaire {
        return Object.assign({ }, questionnaire, { questions });
    }

    private async deleteQuestionsByQuestionnaireId(requestId: string, id: string): Promise<void> {
        return this.questionDao.getQuestionsByQuestionnaireId(requestId, id)
                   .then(data => this.deleteQuestions(requestId, data.map(({ id }) => id)));
    }

    private static buildAnswer(answer: IAnswerData, nextQuestions: Array<IQuestionData>): IAnswer {
        return Object.assign({} , answer, { nextQuestions });
    }
}
