import { ILogger } from '../../logger/console_logger';
import { buildMultiplyPattern } from './sql_utils';
import { ISqlClient } from './sql_types';
import * as uuid from 'uuid';

export interface IAnswerData {
    id: string;
    text: string;
    questionId: string;
}

interface IAnswerDbData {
    id: string;
    text: string;
    question_id: string;
}

export interface IAnswersDao {
    getAnswerById(requestId: string, id: string): Promise<IAnswerData>;
    getAnswersById(requestId: string, ids: Array<string>): Promise<Array<IAnswerData>>;
    getAnswersByQuestionId(requestId: string, id: string): Promise<Array<IAnswerData>>;
    deleteAnswer(requestId: string, id: string): Promise<void>;
    deleteAnswers(requestId: string, ids: Array<string>): Promise<void>;
    updateAnswer(requestId: string, answer: IAnswerData): Promise<void>;
    createAnswer(requestId: string, answer: IAnswerData): Promise<IAnswerData>;
    deleteAnswersByQuestionId(requestId: string, id: string): Promise<void>;
}

export class AnswersDao implements IAnswersDao {
    constructor(private logger: ILogger, private sqlClient: ISqlClient) {
    }

    async getAnswersByQuestionId(requestId: string, questionId: string): Promise<Array<IAnswerData>> {
        this.logger.trace({ requestId, description: 'About to get answers by question questionId', questionId });
        try {
            return this.sqlClient.query<IAnswerDbData>('SELECT * FROM answers WHERE question_id=?;', [questionId])
                       .then(list => list.map(a => this.buildAnswer(a)));
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot get answers by question questionId', questionId, err });
            throw err;
        }
    }

    async createAnswer(requestId: string, answer: IAnswerData): Promise<IAnswerData> {
        this.logger.trace({ requestId, description: 'About to create the answer', answer });
        if (answer == null) {
            return;
        }
        try {
            answer.id = uuid.v1();
            return this.sqlClient.query('INSERT INTO answers (id, text, question_id) VALUES (?, ?, ?);', [answer.id, answer.text, answer.questionId])
                       .then(() => answer);
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot create answer', answer, err });
            throw err;
        }
    }

    async deleteAnswers(requestId: string, ids: Array<string>): Promise<void> {
        this.logger.trace({ requestId, description: 'About to delete the answers', ids });
        if (!ids.length) {
            return;
        }
        try {
            return this.sqlClient.query(`DELETE FROM answers WHERE id IN (${buildMultiplyPattern(ids)});`, ids)
                       .then();
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot delete answers', ids, err });
            throw err;
        }
    }

    async getAnswerById(requestId: string, id: string): Promise<IAnswerData> {
        return this.getAnswersById(requestId, [id])
                   .then(data => data?.[0]);
    }

    async getAnswersById(requestId: string, ids: Array<string>): Promise<Array<IAnswerData>> {
        this.logger.trace({ requestId, description: 'About to get answers', ids });
        if (!ids.length) {
            return [];
        }
        try {
            return this.sqlClient.query<IAnswerDbData>(`SELECT * FROM answers WHERE id IN (${buildMultiplyPattern(ids)});`, ids)
                       .then(list => list.map(a => this.buildAnswer(a)));
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot get answers', ids, err});
            throw err;
        }
    }

    async updateAnswer(requestId: string, answer: IAnswerData): Promise<void> {
        this.logger.trace({ requestId, description: 'About to update the answer', answer });
        if (answer == null) {
            return;
        }
        try {
            return this.sqlClient.query('UPDATE answers SET text=? WHERE id=?;', [answer.text, answer.id])
                       .then();
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot update the answer', answer, err });
            throw err;
        }
    }

    async deleteAnswer(requestId: string, id: string): Promise<void> {
        return await this.deleteAnswers(requestId, [id]);
    }

    async deleteAnswersByQuestionId(requestId: string, id: string): Promise<void> {
        this.logger.trace({ requestId, description: 'About to delete answers by Question ID', id });
        try {
            return this.sqlClient.query('DELETE FROM answers WHERE question_id=?;', [id])
                       .then();
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot delete answers by Question ID', id, err });
            throw err;
        }
    }

    private buildAnswer(data: IAnswerDbData): IAnswerData {
        return {
            id: data.id,
            text: data.text,
            questionId: data.question_id
        }
    }
}