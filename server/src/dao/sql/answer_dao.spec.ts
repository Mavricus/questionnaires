import { AnswersDao } from './answer_dao';
import { SqlClientStub } from '../../../stubs/sql_utils';
import * as sinon from 'sinon';
import { LoggerStub } from '../../../stubs/logger_stub';

describe('AnswersDao', () => {
    let dao: AnswersDao;
    let sqlClient: SqlClientStub;
    let logger: LoggerStub;
    const formattedQuery = 'Test formatted query string';
    const sqlClientResult = ['Answer 1', 'Answer 2'];
    const requestId = 'Test request';

    beforeEach(() => {
        logger = new LoggerStub(sinon);
        logger.defineDefaultBehaviour();

        sqlClient = new SqlClientStub(sinon);
        sqlClient.stubs.query.resolves(sqlClientResult);

        dao = new AnswersDao(logger, sqlClient);
    });

    describe('createAnswer', () => {
        const testAnswer = { id: '123', text: 'Test question', questionId: '1' };
        
        it('creates new answer and returns true iin positive case', async () => {
            await expect(dao.createAnswer(requestId, testAnswer)).resolves.toBeUndefined();
        });

        it('throws an error if query cannot be formatted', async () => {
            const err = new Error('Test exception');
            sqlClient.stubs.query.rejects(err);
            await expect(dao.createAnswer(requestId, testAnswer)).rejects.toBe(err);
        });

        it('does not make a DB request if answer is NULL', async () => {
            await expect(dao.createAnswer(requestId, null)).resolves.toBeUndefined();
            await expect(dao.createAnswer(requestId, undefined)).resolves.toBeUndefined();
            expect(sqlClient.stubs.query.called).toBe(false);
        });
    });

    describe('getAnswersByQuestionId', () => {
        const questionId = '123';

        it('returns a query result', async () => {
            await expect(dao.getAnswersByQuestionId(requestId, questionId)).resolves.toBe(sqlClientResult);
        });

        it('throws an error if query cannot be formatted', async () => {
            const err = new Error('Test exception');
            sqlClient.stubs.query.rejects(err);
            await expect(dao.getAnswersByQuestionId(requestId, questionId)).rejects.toBe(err);
        });
    });

    describe('deleteAnswers', () => {
        const questionIds = ['123', '321'];

        it('deletes the answers and returns true', async () => {
            await expect(dao.deleteAnswers(requestId, questionIds)).resolves.toBeUndefined();
        });

        it('throws an error if query cannot be formatted', async () => {
            const err = new Error('Test exception');
            sqlClient.stubs.query.rejects(err);
            await expect(dao.deleteAnswers(requestId, questionIds)).rejects.toBe(err);
        });

        it('does not make a DB request if IDs list is empty', async () => {
            await expect(dao.deleteAnswers(requestId, [])).resolves.toBeUndefined();
            expect(sqlClient.stubs.query.called).toBe(false);
        });
    });

    describe('getAnswersById', () => {
        const questionIds = ['123', '321'];

        it('returns a query result', async () => {
            await expect(dao.getAnswersById(requestId, questionIds)).resolves.toBe(sqlClientResult);
        });

        it('throws an error if query cannot be formatted', async () => {
            const err = new Error('Test exception');
            sqlClient.stubs.query.rejects(err);
            await expect(dao.getAnswersById(requestId, questionIds)).rejects.toBe(err);
        });

        it('does not make a DB request if IDs list is empty', async () => {
            await expect(dao.getAnswersById(requestId, [])).resolves.toEqual([]);
            expect(sqlClient.stubs.query.called).toBe(false);
        });
    });

    describe('updateAnswer', () => {
        const testAnswer = { id: '123', text: 'Test question', questionId: '1' };

        it('updates answer and returns true', async () => {
            await expect(dao.updateAnswer(requestId, testAnswer)).resolves.toBeUndefined();
        });

        it('throws an error if query cannot be formatted', async () => {
            const err = new Error('Test exception');
            sqlClient.stubs.query.rejects(err);
            await expect(dao.updateAnswer(requestId, testAnswer)).rejects.toBe(err);
        });
        
        it('does not make a DB request if answers list is empty', async () => {
            await expect(dao.getAnswersById(requestId, [])).resolves.toEqual([]);
            expect(sqlClient.stubs.query.called).toBe(false);
        });
    });

    describe('deleteAnswer', () => {
        const questionId = '123';

        it('deletes the answer and returns true', async () => {
            await expect(dao.deleteAnswer(requestId, questionId)).resolves.toBeUndefined();
        });

        it('throws an error if query cannot be formatted', async () => {
            const err = new Error('Test exception');
            sqlClient.stubs.query.rejects(err);
            await expect(dao.deleteAnswer(requestId, questionId)).rejects.toBe(err);
        });
    });
});