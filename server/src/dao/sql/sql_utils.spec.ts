import { buildMultiplyPattern } from './sql_utils';

describe('SQL Utils', () => {

    describe('buildMultiplyPattern', () => {
        it('returns a multiple items pattern for the SQL query', () => {
            expect(buildMultiplyPattern([1,2,3,4,5])).toBe('?,?,?,?,?');
        });

        it('handels properly array of 1 element', () => {
            expect(buildMultiplyPattern([1])).toBe('?');
        });

        it('handels properly array of 2 elements', () => {
            expect(buildMultiplyPattern([1])).toBe('?');
        });

        it('throws an error if th array is empty', () => {
            expect(() => buildMultiplyPattern([])).toThrow();
        });
    });
});