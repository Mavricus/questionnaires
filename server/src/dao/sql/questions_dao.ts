import { ILogger } from '../../logger/console_logger';
import { buildMultiplyPattern } from './sql_utils';
import { ISqlClient } from './sql_types';
import * as uuid from 'uuid';

export interface IQuestionData {
    id: string;
    text: string;
    questionnaireId: string;
    isInitial: boolean;
}

interface IQuestionDbData {
    id: string;
    text: string;
    questionnaire_id: string;
    is_initial: number;
}

export interface IQuestionsDao {
    getQuestionsById(requestId: string, ids: Array<string>): Promise<Array<IQuestionData>>;
    getQuestionById(requestId: string, id: string): Promise<IQuestionData>
    deleteQuestion(requestId: string, id: string): Promise<void>;
    deleteQuestions(requestId: string, ids: Array<string>): Promise<void>;
    updateQuestion(requestId: string, question: IQuestionData): Promise<void>;
    createQuestion(requestId: string, question: IQuestionData): Promise<IQuestionData>;
    getQuestionsByQuestionnaireId(requestId: string, id: string): Promise<Array<IQuestionData>>;
}

export class QuestionsDao implements IQuestionsDao {
    constructor(private logger: ILogger, private sqlClient: ISqlClient) {
    }

    async createQuestion(requestId: string, question: IQuestionData): Promise<IQuestionData> {
        this.logger.trace({ requestId, description: 'About to create the question', question });
        try {
            question.id = uuid.v1();
            const isInitial = question.isInitial ? 1 : 0;
            return this.sqlClient.query('INSERT INTO questions (id, text, questionnaire_id, is_initial) VALUES (?,?,?,?);', [question.id, question.text, question.questionnaireId, isInitial])
                       .then(() => question);
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot create question', question, err });
            throw err;
        }
    }

    async deleteQuestions(requestId: string, ids: Array<string>): Promise<void> {
        this.logger.trace({ requestId, description: 'About to delete questions', ids });
        try {
            let tasks = ids.map(id => this.deleteQuestion(requestId, id));
            await Promise.all(tasks);
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot delete questions', ids, err });
            throw err;
        }
    }

    async getQuestionById(requestId: string, id: string): Promise<IQuestionData> {
        return this.getQuestionsById(requestId, [id])
                   .then(([result]) => result);
    }

    async getQuestionsById(requestId: string, ids: Array<string>): Promise<Array<IQuestionData>> {
        this.logger.trace({ requestId, description: 'About to get questions', ids });
        if (!ids.length) {
            return [];
        }
        try {
            return this.sqlClient.query<IQuestionDbData>(`SELECT * FROM questions WHERE id IN (${buildMultiplyPattern(ids)});`, [ids])
                       .then(list => list.map(item => this.buildQuestion(item)));
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot get questions', ids, err });
            throw err;
        }
    }

    async updateQuestion(requestId: string, question: IQuestionData): Promise<void> {
        this.logger.trace({ requestId, description: 'About to update questions', question });
        try {
            const isInitial = question.isInitial ? 1 : 0;
            await this.sqlClient.query('UPDATE questions SET text=?, is_initial=? WHERE id=?;', [question.text, isInitial, question.id]);
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot update the question', question, err });
            throw err;
        }
    }

    async deleteQuestion(requestId: string, id: string): Promise<void> {
        this.logger.trace({ requestId, description: 'About to delete question', id });
        try {
            await this.sqlClient.query('DELETE FROM questions WHERE id=?;', [id]);
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot delete question', id, err });
            throw err;
        }
    }

    getQuestionsByQuestionnaireId(requestId: string, id: string): Promise<Array<IQuestionData>> {
        this.logger.trace({ requestId, description: 'About to get questions by questionnaire ID', id });
        try {
            return this.sqlClient.query<IQuestionDbData>('SELECT * FROM questions WHERE questionnaire_id=?;', [id])
                       .then(list => list.map(item => this.buildQuestion(item)));
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot get questions by questionnaire ID', id, err });
            throw err;
        }
    }

    private buildQuestion(data: IQuestionDbData): IQuestionData {
        return {
            id: data.id,
            isInitial: data.is_initial !== 0,
            questionnaireId: data.questionnaire_id,
            text: data.text
        }
    }
}
