import { ILogger } from '../../logger/console_logger';
import { ISqlClient } from './sql_types';
import * as uuid from 'uuid';

export interface IQuestionnaireData {
    id: string;
    description: string;
}

export interface IQuestionnairesDao {
    getAllQuestionnaires(requestId: string): Promise<Array<IQuestionnaireData>>;
    createQuestionnaire(requestId: string, questionnaire: IQuestionnaireData): Promise<IQuestionnaireData>;
    getQuestionnaireById(requestId: string, id: string): Promise<IQuestionnaireData>;
    deleteQuestionnaire(requestId: string, id: string): Promise<void>;
    updateQuestionnaire(requestId: string, questionnaire: IQuestionnaireData): Promise<void>;
}

export class QuestionnairesDao implements IQuestionnairesDao {
    constructor(private logger: ILogger, private sqlClient: ISqlClient) {
    }

    async getAllQuestionnaires(requestId: string): Promise<Array<IQuestionnaireData>> {
        try {
            this.logger.trace({ requestId, description: 'About to get all questionnaires' });
            return this.sqlClient.query('SELECT * FROM questionnaires', []);
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot get all questionnaires', err });
            throw err;
        }
    }

    async createQuestionnaire(requestId: string, questionnaire: IQuestionnaireData): Promise<IQuestionnaireData> {
        try {
            questionnaire.id = uuid.v1();
            this.logger.trace({ requestId, description: 'About to create a questionnaire', questionnaire });
            return this.sqlClient.query('INSERT INTO questionnaires (id, description) VALUES (?,?)', [questionnaire.id, questionnaire.description])
                       .then(() => questionnaire);
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot create a questionnaire', questionnaire, err });
            throw err;
        }
    }

    async getQuestionnaireById(requestId: string, id: string): Promise<IQuestionnaireData> {
        try {
            this.logger.trace({ requestId, description: 'About to get questionnaire by id', id });
            return this.sqlClient.query<IQuestionnaireData>('SELECT * FROM questionnaires WHERE id=?', [id])
                       .then(([result]) => result);
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot get questionnaire by id', id, err });
            throw err;
        }
    }

    async deleteQuestionnaire(requestId: string, id: string): Promise<void> {
        try {
            this.logger.trace({ requestId, description: 'About to delete questionnaire', id });
            return this.sqlClient.query<IQuestionnaireData>('DELETE FROM questionnaires WHERE id=?;', [id]).then(()=> null);
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot delete questionnaire', id, err });
            throw err;
        }
    }

    async updateQuestionnaire(requestId: string, questionnaire: IQuestionnaireData): Promise<void> {
        try {
            this.logger.trace({ requestId, description: 'About to update questionnaire', questionnaire });
            return this.sqlClient.query<IQuestionnaireData>('UPDATE questionnaires SET description=? WHERE id=?;', [questionnaire.description, questionnaire.id])
                       .then();
        } catch (err) {
            this.logger.error({ requestId, description: 'Cannot update questionnaire', questionnaire, err });
            throw err;
        }
    }
}