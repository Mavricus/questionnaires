export function buildMultiplyPattern(list: Array<any>): string {
    if (!list.length) {
        throw new RangeError('List cannot be empty');
    }
    return '?,'.repeat(list.length - 1) + '?';
}