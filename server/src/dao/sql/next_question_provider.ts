import { ILogger } from '../../logger/console_logger';
import { ISqlClient } from './sql_types';
import { buildMultiplyPattern } from './sql_utils';

interface INextQuestionDbData {
    answer_id: string;
    question_id: string;
}

export interface INextQuestionData {
    answerId: string;
    questionId: string;
}

export interface INextQuestionsProvider {
    get(requestId: string, answers: Array<string>): Promise<Array<string>>;
    create(requestId: string, data: INextQuestionData): Promise<INextQuestionData>;
    delete(requestId: string, data: INextQuestionData): Promise<void>;
    deleteByQuestionId(requestId: string, questionId: string): Promise<void>;
    deleteByAnswerId(requestId: string, answerId: string): Promise<void>;
}

export class NextQuestionsProviderSql implements INextQuestionsProvider {
    constructor(private logger: ILogger, private sqlClient: ISqlClient) {
    }
    async get(requestId: string, answers: Array<string>): Promise<Array<string>> {
        this.logger.trace({ requestId, description: 'About to get next questions for the answers', answers });
        try {
            return this.sqlClient.query<INextQuestionDbData>(`SELECT * FROM next_questions WHERE answer_id IN (${buildMultiplyPattern(answers)});`, answers)
                       .then(nextQuestions => Array.from(new Set(nextQuestions.map(q => q.question_id))));
        } catch (err) {
            this.logger.warn({ description: 'Cannot get next questions', answers, err, requestId });
            throw err;
        }
    }

    async create(requestId: string, data: INextQuestionData): Promise<INextQuestionData> {
        this.logger.trace({ requestId, description: 'About to add next question', data });
        try {
            return this.sqlClient.query<void>(`INSERT INTO next_questions (answer_id, question_id) VALUES (?, ?);`, [data.answerId, data.questionId])
                      .then(() => data);
        } catch (err) {
            this.logger.warn({ description: 'Cannot add next question', data, err, requestId });
            throw err;
        }
    }

    async delete(requestId: string, data: INextQuestionData): Promise<void> {
        this.logger.trace({ requestId, description: 'About to delete next question', data });
        try {
            return this.sqlClient.query<void>(`DELETE FROM next_questions WHERE answer_id=? AND question_id=?;`, [data.answerId, data.questionId])
                       .then();
        } catch (err) {
            this.logger.warn({ description: 'Cannot delete next question', data, err, requestId });
            throw err;
        }
    }

    async deleteByAnswerId(requestId: string, answerId: string): Promise<void> {
        this.logger.trace({ requestId, description: 'About to delete next question', answerId });
        try {
            return this.sqlClient.query<void>('DELETE FROM next_questions WHERE answer_id=?;', [answerId])
                       .then();
        } catch (err) {
            this.logger.warn({ description: 'Cannot delete next question', answerId, err, requestId });
            throw err;
        }
    }

    async deleteByQuestionId(requestId: string, questionId: string): Promise<void> {
        this.logger.trace({ requestId, description: 'About to delete next question', questionId });
        try {
            return this.sqlClient.query<void>('DELETE FROM next_questions WHERE question_id=?;', [questionId])
                       .then();
        } catch (err) {
            this.logger.warn({ description: 'Cannot delete next question', questionId, err, requestId });
            throw err;
        }
    }
}