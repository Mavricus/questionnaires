import { SqlClientStub } from '../../../stubs/sql_utils';
import { LoggerStub } from '../../../stubs/logger_stub';
import { QuestionsDao } from './questions_dao';
import * as sinon from 'sinon';

describe('QuestionsDao', () => {
    let dao: QuestionsDao;
    let sqlClient: SqlClientStub;
    let logger: LoggerStub;
    const sqlClientResult = ['Question 1', 'Question 2'];
    const requestId = 'Test request';

    beforeEach(() => {
        logger = new LoggerStub(sinon);
        logger.defineDefaultBehaviour();

        sqlClient = new SqlClientStub(sinon);
        sqlClient.stubs.query.resolves(sqlClientResult);

        dao = new QuestionsDao(logger, sqlClient);
    });

    describe('getQuestionsById', () => {
        const ids = ['123', '321'];

        it('returns list of questions', async () => {
            await expect(dao.getQuestionsById(requestId, ids)).resolves.toBe(sqlClientResult);
        });

        it('rejects if query to DB failed', async () => {
            const err = new Error('test exception');
            sqlClient.stubs.query.rejects(err);

            await expect(dao.getQuestionsById(requestId, ids)).rejects.toBe(err);
        });

        it('does not make DB request if answers list is empty', async () => {
            await expect(dao.getQuestionsById(requestId, [])).resolves.toEqual([]);
            expect(sqlClient.stubs.query.called).toBe(false);
        });
    });

    describe('getQuestionById', () => {
        const id = '123';

        it('returns the first result of the query', async () => {
            await expect(dao.getQuestionById(requestId, id)).resolves.toBe(sqlClientResult[0]);
        });

        it('rejects if query to DB failed', async () => {
            const err = new Error('test exception');
            sqlClient.stubs.query.rejects(err);

            await expect(dao.getQuestionById(requestId, id)).rejects.toBe(err);
        });
    });

    describe('deleteQuestion', () => {
        const id = '123';

        it('not throws if question was deleted successfully', async () => {
            await expect(dao.deleteQuestion(requestId, id)).resolves.toBeUndefined();
        });

        it('rejects if query to DB failed', async () => {
            const err = new Error('test exception');
            sqlClient.stubs.query.rejects(err);

            await expect(dao.deleteQuestion(requestId, id)).rejects.toBe(err);
        });
    });
});