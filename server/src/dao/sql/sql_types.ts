export interface ISqlClient {
    query<T>(template: string, args: Array<any>): Promise<Array<T>>;
}