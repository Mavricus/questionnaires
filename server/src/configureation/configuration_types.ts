export interface IAppConfig {
    server: {
        port: number;
        hostname: string;
    };
    db: {
        questions: {
            path: string;
        };
    };
}

export interface IConfigProvider {
    getConfig(): IAppConfig;
}

export interface IConfigReader extends IConfigProvider {
    renewConfig(): Promise<IAppConfig>;
}