import { IAppConfig, IConfigReader } from './configuration_types';
import { readFile, readFileSync } from 'fs';
import { promisify } from 'util';

interface IFs {
    readFile: typeof readFile;
    readFileSync: typeof readFileSync;
}

export class FsConfigReader implements IConfigReader {
    private config: IAppConfig;

    constructor(private path: string, private fs: IFs) {
        this.config = JSON.parse(this.fs.readFileSync(this.path, 'utf-8'));
    }

    getConfig(): IAppConfig {
        return this.config;
    }

    renewConfig(): Promise<IAppConfig> {
        return promisify(this.fs.readFile)(this.path, 'utf-8').then(text => JSON.parse(text));
    }
}