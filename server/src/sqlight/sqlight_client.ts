import { ISqlClient } from '../dao/sql/sql_types';
import { Database } from 'sqlite3';
import { IConfigProvider } from '../configureation/configuration_types';

export class SqlightClient implements ISqlClient {
    constructor(private db: Database) {
    }
    
    query<T>(template: string, args: Array<any>): Promise<Array<T>> {
        return new Promise((resolve, reject) => {
            let query = this.db.prepare(template, ...args, (err) => {
                if (err) {
                    return reject(err);
                }
                query.all((err, data) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(data);
                });
            });
        });
    }
}

export class SqlightDbFactory {
    private client: SqlightClient | null = null;
    private path: string = '';
    
    constructor(private pathProvider: () => string) {
    }

    getProvider(): SqlightClient {
        let path = this.pathProvider();
        if (this.client == null || this.path !== path) {
            this.path = path;
            this.client = new SqlightClient(new Database(path));
        }
        return this.client;
    }
}