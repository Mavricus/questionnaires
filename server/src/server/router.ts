import { Router } from 'express';
import { IAllHandlerFactory } from './routes/router_types';
import { IQuestionRouter } from './routes/questionnaire/question_route';
import { IAnswerRouter } from './routes/questionnaire/answer_route';
import { IQuestionnaireRoute } from './routes/questionnaire/questionnaire_route';
import { INextQuestionRoute } from './routes/questionnaire/next_question_route';

export interface IServerRouterFactory {
    create(): Router;
}

export class ServerRouterFactory implements IServerRouterFactory {
    constructor(private proxy: IAllHandlerFactory,
                private notFoundRoute: IAllHandlerFactory,
                private questionnaireQuestionsRoute: IQuestionnaireRoute,
                private questionsRoute: IQuestionRouter,
                private answersRoute: IAnswerRouter,
                private nextQuestion: INextQuestionRoute) {
    }

    create(): Router {
        let router = Router();
        router.all('*', this.proxy.createAllHandler());

        router.get('/questionnaires', this.questionnaireQuestionsRoute.createGetAllHandler());

        router.post('/questionnaire', this.questionnaireQuestionsRoute.createPostHandler());
        router.get('/questionnaire/:questionnaireId', this.questionnaireQuestionsRoute.createGetHandler());
        router.delete('/questionnaire/:questionnaireId', this.questionnaireQuestionsRoute.createDeleteHandler());
        router.patch('/questionnaire/:questionnaireId', this.questionnaireQuestionsRoute.createPatchHandler());
        router.get('/questionnaire/:questionnaireId/questions', this.questionnaireQuestionsRoute.createGetByQuestionnaireIdHandler());

        router.post('/question', this.questionsRoute.createPostHandler());
        router.get('/question/:questionId', this.questionsRoute.createGetHandler());
        router.delete('/question/:questionId', this.questionsRoute.createDeleteHandler());
        router.patch('/question/:questionId', this.questionsRoute.createPatchHandler());

        router.post('/answer', this.answersRoute.createPostHandler());
        router.get('/answer/:answerId', this.answersRoute.createGetHandler());
        router.delete('/answer/:answerId', this.answersRoute.createDeleteHandler());
        router.patch('/answer/:answerId', this.answersRoute.createPatchHandler());

        router.post('/nextQuestion', this.nextQuestion.createPostHandler());
        router.delete('/nextQuestion/:answerId/:questionId', this.nextQuestion.createDeleteHandler());

        router.all('*', this.notFoundRoute.createAllHandler());
        return router;
    }
}