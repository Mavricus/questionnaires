import { Express } from 'express';
import { IConfigProvider } from '../configureation/configuration_types';
import { promisify } from 'util';

export interface IServer {
    start(): Promise<IServer>;
}

export class Server implements IServer {
    constructor(private app: Express, private configProvider: IConfigProvider) {
    }

    async start(): Promise<IServer> {
        let { server } = this.configProvider.getConfig();
        return promisify((callback) => this.app.listen(server.port, server.hostname, callback))().then(() => this);
    }

}