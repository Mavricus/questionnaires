import { IServerRouterFactory } from './router';
import { Express } from 'express';
import { IConfigProvider } from '../configureation/configuration_types';
import * as express from 'express';
import { ILogger } from '../logger/console_logger';
import { IServer, Server } from './server';
import * as cors from 'cors';

export class AppServerFactory {
    constructor(private configProvider: IConfigProvider, private routerFactory: IServerRouterFactory, private logger: ILogger) {
    }

    create(): IServer {
        let app: Express = express();

        app.use(cors());
        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));

        app.use(this.routerFactory.create());
        return new Server(app, this.configProvider);
    }
}
