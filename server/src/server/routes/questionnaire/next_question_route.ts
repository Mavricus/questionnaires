import { IDeleteHandlerFactory, IGetHandlerFactory, IPatchHandlerFactory, IPostHandlerFactory } from '../router_types';
import { RequestHandler } from 'express';
import { IQuestionnairesProvider } from '../../../dao/questionnaires_provider';
import { logger } from '../../../../app';

export interface INextQuestionRoute extends IPostHandlerFactory, IDeleteHandlerFactory {

}

export class NextQuestionRoute implements INextQuestionRoute {
    constructor(private questionsProvider: IQuestionnairesProvider) {
    }

    createDeleteHandler(): RequestHandler {
        return (req, res) => {
            const requestId = <string>req.headers.requestId;
            const { answerId, questionId } = req.params;
            this.questionsProvider.deleteNextQuestion(requestId, { answerId, questionId }).then(() => {
                res.statusCode = 204;
                res.json();
            }).catch((err: Error) => {
                const { message, stack } = err;
                res.statusCode = 500;
                res.json({ exceptionMessage: message, stack, method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }
    
    createPostHandler(): RequestHandler {
        return (req, res) => {
            const requestId = <string>req.headers.requestId;
            this.questionsProvider.createNextQuestion(requestId, req.body).then(nextQuestion => {
                res.statusCode = 201;
                res.json({ nextQuestion });
            }).catch((err: Error) => {
                const { message, stack } = err;
                res.statusCode = 500;
                res.json({ exceptionMessage: message, stack, method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }
}