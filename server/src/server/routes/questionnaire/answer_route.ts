import { IDeleteHandlerFactory, IGetHandlerFactory, IPatchHandlerFactory, IPostHandlerFactory } from '../router_types';
import { RequestHandler } from 'express';
import { logger } from '../../../../app';
import { IAnswersDao } from '../../../dao/sql/answer_dao';
import { IQuestionnairesProvider } from '../../../dao/questionnaires_provider';

export interface IAnswerRouter extends IGetHandlerFactory, IPostHandlerFactory, IDeleteHandlerFactory, IPatchHandlerFactory {

}

export class AnswerRouter implements IAnswerRouter {
    constructor(private questionsProvider: IQuestionnairesProvider) {
    }
    
    createGetHandler(): RequestHandler {
        return (req, res) => {
            const requestId = <string>req.headers.requestId;
            this.questionsProvider.getAnswerById(requestId, req.params.answerId).then((answer) => {
                res.statusCode = 200;
                res.json({ answer });
            }).catch(err => {
                res.statusCode = 500;
                res.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }

    createPostHandler(): RequestHandler {
        return (req, res) => {
            const requestId = <string>req.headers.requestId;
            this.questionsProvider.createAnswer(requestId, req.body).then((answer) => {
                res.statusCode = 201;
                res.json({ answer });
            }).catch(err => {
                res.statusCode = 500;
                res.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }

    createDeleteHandler(): RequestHandler {
        return (req, res) => {
            const requestId = <string>req.headers.requestId;
            this.questionsProvider.deleteAnswer(requestId, req.params.answerId).then(() => {
                res.statusCode = 204;
                res.json();
            }).catch(err => {
                res.statusCode = 500;
                res.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }

    createPatchHandler(): RequestHandler {
        return (req, res) => {
            const requestId = <string>req.headers.requestId;
            this.questionsProvider.updateAnswer(requestId, req.body).then(() => {
                res.statusCode = 204;
                res.json();
            }).catch(err => {
                res.statusCode = 500;
                res.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }
}
