import { IDeleteHandlerFactory, IGetHandlerFactory, IPatchHandlerFactory, IPostHandlerFactory } from '../router_types';
import { RequestHandler } from 'express';
import { IQuestionnairesProvider } from '../../../dao/questionnaires_provider';
import { logger } from '../../../../app';

export interface IQuestionRouter extends IPostHandlerFactory, IGetHandlerFactory, IDeleteHandlerFactory, IPatchHandlerFactory {

}

export class QuestionRoute implements IQuestionRouter {
    constructor(private questionsProvider: IQuestionnairesProvider) {
    }

    createPostHandler(): RequestHandler {
        return (req, res) => {
            const requestId = <string>req.headers.requestId;
            const data = req.body;
            this.questionsProvider.createQuestion(requestId, data).then((question) => {
                res.statusCode = 201;
                res.json({ question });
            }).catch(err => {
                res.statusCode = 500;
                res.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }

    createGetHandler(): RequestHandler {
        return (req, res) => {
            const requestId = <string>req.headers.requestId;
            this.questionsProvider.getQuestionDetails(requestId, req.params.questionId).then((question) => {
                res.statusCode = 200;
                res.json({ question });
            }).catch(err => {
                res.statusCode = 500;
                res.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }

    createDeleteHandler(): RequestHandler {
        return (req, res) => {
            const requestId = <string>req.headers.requestId;
            this.questionsProvider.deleteQuestion(requestId, req.params.questionId).then(() => {
                res.statusCode = 204;
                res.json();
            }).catch(err => {
                res.statusCode = 500;
                res.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }

    createPatchHandler(): RequestHandler {
        return (req, res) => {
            const requestId = <string>req.headers.requestId;
            const data = req.body;
            this.questionsProvider.updateQuestion(requestId, data).then(() => {
                res.statusCode = 204;
                res.json();
            }).catch(err => {
                res.statusCode = 500;
                res.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }
}