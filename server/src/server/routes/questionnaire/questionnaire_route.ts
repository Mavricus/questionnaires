import { IDeleteHandlerFactory, IGetHandlerFactory, IPatchHandlerFactory, IPostHandlerFactory } from '../router_types';
import { RequestHandler } from 'express';
import { logger } from '../../../../app';
import { IQuestionnairesProvider } from '../../../dao/questionnaires_provider';

export interface IQuestionnaireRoute extends IGetHandlerFactory, IPostHandlerFactory, IDeleteHandlerFactory, IPatchHandlerFactory {
    createGetByQuestionnaireIdHandler(): RequestHandler;
    createGetAllHandler(): RequestHandler;
}

export class QuestionnaireRoute implements IQuestionnaireRoute {
    constructor(private questionnairesProvider: IQuestionnairesProvider) {
    }

    createGetHandler(): RequestHandler {
        return (req, resp) => {
            const requestId = <string>req.headers.requestId;
            this.questionnairesProvider.getQuestionnaireDetails(requestId, req.params.questionnaireId)
                .then(data => {
                    resp.statusCode = 200;
                    resp.json({ questionnaire: data });
                }).catch(err => {
                    resp.statusCode = 500;
                    resp.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                    logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
                });
        }
    }

    createPostHandler(): RequestHandler {
        return (req, resp) => {
            const requestId = <string>req.headers.requestId;

            this.questionnairesProvider.createQuestionnaire(requestId, req.body)
                .then(questionnaire => {
                    resp.statusCode = 201;
                    resp.json({ questionnaire });
                }).catch(err => {
                resp.statusCode = 500;
                resp.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        };
    }

    createDeleteHandler(): RequestHandler {
        return (req, resp) => {
            const requestId = <string>req.headers.requestId;
            this.questionnairesProvider.deleteQuestionnaire(requestId, req.params.questionnaireId)
                .then(() => {
                    resp.statusCode = 204;
                    resp.json();
                })
                .catch(err => {
                    resp.statusCode = 500;
                    resp.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                    logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
                });
        }
    }

    createPatchHandler(): RequestHandler {
        return (req, resp) => {
            const requestId = <string>req.headers.requestId;
            this.questionnairesProvider.updateQuestionnaire(requestId, req.body)
                .then(() => {
                    resp.statusCode = 204;
                    resp.json();
                })
                .catch(err => {
                    resp.statusCode = 500;
                    resp.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                    logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
                });
        }
    }

    createGetByQuestionnaireIdHandler(): RequestHandler {
        return (req, res) => {
            let requestId = req.params.requestId;
            this.questionnairesProvider.getQuestionsByQuestionnaireId(requestId, req.params.questionnaireId)
                .then(data => {
                    res.statusCode = 200;
                    res.json({ questions: data });
                }).catch(err => {
                    res.statusCode = 500;
                    res.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                    logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
                });
        };
    }

    createGetAllHandler(): RequestHandler {
        return (req, res) => {
            let requestId = req.params.requestId;
            this.questionnairesProvider.getAllQuestionnaires(requestId)
                .then(data => {
                    res.statusCode = 200;
                    res.json({ questionnaires: data });
                }).catch(err => {
                res.statusCode = 500;
                res.json({ exceptionMessage: 'Cannot process request', method: req.method, path: req.path, requestId });
                logger.warn({ requestId, description: 'Cannot process request', details: { method: req.method, path: req.path }, err });
            });
        }
    }
}