import { RequestHandler } from 'express';
import { ILogger } from '../../logger/console_logger';
import { IAllHandlerFactory } from './router_types';
import * as uuid from 'uuid';

export interface IProxyRouter extends IAllHandlerFactory {
}

export class ProxyRouter implements IAllHandlerFactory {
    constructor(private logger: ILogger) {
    }

    createAllHandler(): RequestHandler {
        return (req, rest, next) => {
            if (req.headers.requestId == null) {
                req.headers.requestId = uuid.v1();
            }
            let { headers, params, path, body } = req;
            let requestId = <string>headers.requestId;
            this.logger.info({ description: 'New src request', requestId, headers, path, params, body });
            return next();
        };
    }
}