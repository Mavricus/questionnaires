import { RequestHandler } from 'express';

export interface IGetHandlerFactory {
    createGetHandler(): RequestHandler;
}

export interface IPostHandlerFactory {
    createPostHandler(): RequestHandler;
}

export interface IPatchHandlerFactory {
    createPatchHandler(): RequestHandler;
}

export interface IDeleteHandlerFactory {
    createDeleteHandler(): RequestHandler;
}

export interface IAllHandlerFactory {
    createAllHandler(): RequestHandler;
}