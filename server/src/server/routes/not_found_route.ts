import { IAllHandlerFactory } from './router_types';
import { RequestHandler } from 'express';

export class NotFoundRoute implements IAllHandlerFactory {
    createAllHandler(): RequestHandler {
        return (req, res) => {
            res.statusCode = 404;
            res.json({ exceptionMessage: 'Cannot find requested resource', method: req.method, path: req.path });
        };
    }

}