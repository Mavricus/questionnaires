import { ILogger, ILogMessage } from '../src/logger/console_logger';
import { SinonStatic, SinonStub } from 'sinon';

export class LoggerStub implements ILogger {
    stubs: {
        debug: SinonStub;
        emergency: SinonStub;
        error: SinonStub;
        info: SinonStub;
        trace: SinonStub;
        warn: SinonStub;
        setLoglevel: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            setLoglevel: sinon.stub(),
            debug: sinon.stub(),
            emergency: sinon.stub(),
            error: sinon.stub(),
            info: sinon.stub(),
            trace: sinon.stub(),
            warn: sinon.stub()
        };
    }

    debug<T extends ILogMessage>(message: T) {
        return this.stubs.debug(message);
    }

    emergency<T extends ILogMessage>(message: T) {
        return this.stubs.emergency(message);
    }

    error<T extends ILogMessage>(message: T) {
        return this.stubs.error(message);
    }

    info<T extends ILogMessage>(message: T) {
        return this.stubs.info(message);
    }

    setLoglevel(level: string) {
        return this.stubs.setLoglevel(level);
    }

    trace<T extends ILogMessage>(message: T) {
        return this.stubs.trace(message);
    }

    warn<T extends ILogMessage>(message: T) {
        return this.stubs.warn(message);
    }

    defineDefaultBehaviour() {
        this.stubs.debug.returns(null);
        this.stubs.emergency.returns(null);
        this.stubs.error.returns(null);
        this.stubs.info.returns(null);
        this.stubs.setLoglevel.returns(null);
        this.stubs.trace.returns(null);
        this.stubs.warn.returns(null);
    };
}