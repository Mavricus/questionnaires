import { IQuestionData, IQuestionsDao } from '../../src/dao/sql/questions_dao';
import { SinonStatic, SinonStub } from 'sinon';

export class QuestionsDaoStub implements IQuestionsDao {
    stubs: {
        createQuestion: SinonStub;
        deleteQuestion: SinonStub;
        deleteQuestions: SinonStub;
        getQuestionById: SinonStub;
        getQuestionsById: SinonStub;
        updateQuestion: SinonStub;
        getAllQuestions: SinonStub;
        getQuestionsByQuestionnaireId: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            createQuestion: sinon.stub(),
            deleteQuestion: sinon.stub(),
            deleteQuestions: sinon.stub(),
            getQuestionById: sinon.stub(),
            getQuestionsById: sinon.stub(),
            getAllQuestions: sinon.stub(),
            getQuestionsByQuestionnaireId: sinon.stub(),
            updateQuestion: sinon.stub()
        };
    }

    createQuestion(requestId: string, question: IQuestionData): Promise<IQuestionData> {
        return this.stubs.createQuestion(requestId, question);
    }

    deleteQuestion(requestId: string, id: string): Promise<void> {
        return this.stubs.deleteQuestion(requestId, id);
    }

    deleteQuestions(requestId: string, ids: Array<string>): Promise<void> {
        return this.stubs.deleteQuestions(requestId, ids);
    }

    getQuestionById(requestId: string, id: string): Promise<IQuestionData> {
        return this.stubs.getQuestionById(requestId, id)
    }

    getQuestionsById(requestId: string, ids: Array<string>): Promise<Array<IQuestionData>> {
        return this.stubs.getQuestionsById(requestId, ids);
    }

    updateQuestion(requestId: string, question: IQuestionData): Promise<void> {
        return this.stubs.updateQuestion(requestId, question);
    }

    getAllQuestions(requestId: string): Promise<Array<IQuestionData>> {
        return this.stubs.updateQuestion(requestId);
    }

    getQuestionsByQuestionnaireId(requestId: string, id: string): Promise<Array<IQuestionData>> {
        return this.stubs.getQuestionsByQuestionnaireId(requestId, id);
    }
}