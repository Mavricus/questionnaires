import { IAnswerData, IAnswersDao } from '../../src/dao/sql/answer_dao';
import { SinonStatic, SinonStub } from 'sinon';

export class AnswersDaoStub implements IAnswersDao {
    stubs: {
        createAnswer: SinonStub;
        deleteAnswer: SinonStub;
        deleteAnswers: SinonStub;
        getAnswersById: SinonStub;
        getAnswersByQuestionId: SinonStub;
        updateAnswer: SinonStub;
        deleteAnswersByQuestionId: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            createAnswer: sinon.stub(),
            deleteAnswer: sinon.stub(),
            deleteAnswers: sinon.stub(),
            getAnswersById: sinon.stub(),
            getAnswersByQuestionId: sinon.stub(),
            deleteAnswersByQuestionId: sinon.stub(),
            updateAnswer: sinon.stub()
        };
    }

    createAnswer(requestId: string, answer: IAnswerData): Promise<IAnswerData> {
        return this.stubs.createAnswer(requestId, answer);
    }

    deleteAnswer(requestId: string, id: string): Promise<void> {
        return this.stubs.deleteAnswer(requestId, id);
    }

    deleteAnswers(requestId: string, ids: Array<string>): Promise<void> {
        return this.stubs.deleteAnswers(requestId, ids);
    }

    getAnswerById(requestId: string, id: string): Promise<IAnswerData> {
        return this.stubs.getAnswersById(requestId, id);
    }

    getAnswersById(requestId: string, ids: Array<string>): Promise<Array<IAnswerData>> {
        return this.stubs.getAnswersById(requestId, ids);
    }

    getAnswersByQuestionId(requestId: string, id: string): Promise<Array<IAnswerData>> {
        return this.stubs.getAnswersByQuestionId(requestId, id);
    }

    updateAnswer(requestId: string, answer: IAnswerData): Promise<void> {
        return this.stubs.updateAnswer(requestId, answer);
    }

    deleteAnswersByQuestionId(requestId: string, id: string): Promise<void> {
        return this.stubs.deleteAnswersByQuestionId(requestId, id);
    }
}