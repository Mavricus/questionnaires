import { INextQuestionData, INextQuestionsProvider } from '../../src/dao/sql/next_question_provider';
import { SinonStatic, SinonStub } from 'sinon';

export class NextQuestionsProviderStub implements INextQuestionsProvider {
    stubs : {
        get: SinonStub;
        create: SinonStub;
        delete: SinonStub;
        update: SinonStub;
        deleteByAnswerId: SinonStub;
        deleteByQuestionId: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            get: sinon.stub(),
            create: sinon.stub(),
            delete: sinon.stub(),
            update: sinon.stub(),
            deleteByAnswerId: sinon.stub(),
            deleteByQuestionId: sinon.stub()
        };
    }

    get(requestId: string, answers: Array<string>): Promise<Array<string>> {
        return this.stubs.get(requestId, answers);
    }

    create(requestId: string, data: INextQuestionData): Promise<INextQuestionData> {
        return this.stubs.create(requestId, data);
    }

    delete(requestId: string, data: INextQuestionData): Promise<void> {
        return this.stubs.delete(requestId, data);
    }

    deleteByAnswerId(requestId: string, questionId: string): Promise<void> {
        return this.stubs.deleteByAnswerId(requestId, questionId);
    }

    deleteByQuestionId(requestId: string, answerId: string): Promise<void> {
        return this.stubs.deleteByQuestionId(requestId, answerId);
    }
}