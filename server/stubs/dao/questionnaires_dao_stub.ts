import { IQuestionnaireData, IQuestionnairesDao } from '../../src/dao/sql/questionnaires_dao';
import { SinonStatic, SinonStub } from 'sinon';

export class QuestionnairesDaoStub implements IQuestionnairesDao {
    stubs: {
        getAllQuestionnaires: SinonStub;
        createQuestionnaire: SinonStub;
        deleteQuestionnaire: SinonStub;
        getQuestionnaire: SinonStub;
        updateQuestionnaire: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            getAllQuestionnaires: sinon.stub(),
            deleteQuestionnaire: sinon.stub(),
            getQuestionnaire: sinon.stub(),
            updateQuestionnaire: sinon.stub(),
            createQuestionnaire: sinon.stub()
        };
    }

    getAllQuestionnaires(requestId: string): Promise<Array<IQuestionnaireData>> {
        return this.stubs.getAllQuestionnaires(requestId);
    }

    createQuestionnaire(requestId: string, questionnaire: IQuestionnaireData): Promise<IQuestionnaireData> {
        return this.stubs.createQuestionnaire(requestId, questionnaire);
    }

    deleteQuestionnaire(requestId: string, id: string): Promise<void> {
        return this.stubs.deleteQuestionnaire(requestId, id);
    }

    getQuestionnaireById(requestId: string, id: string): Promise<IQuestionnaireData> {
        return this.stubs.getQuestionnaire(requestId, id);
    }

    updateQuestionnaire(requestId: string, questionnaire: IQuestionnaireData): Promise<void> {
        return this.stubs.updateQuestionnaire(requestId, questionnaire);
    }
}