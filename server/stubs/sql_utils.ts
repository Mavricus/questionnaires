import { ISqlClient } from '../src/dao/sql/sql_types';
import { SinonStatic, SinonStub } from 'sinon';
import * as util from 'util';

export class SqlClientStub implements ISqlClient {
    stubs: {
        query: SinonStub;
    };

    constructor(sinon: SinonStatic) {
        this.stubs = {
            query: sinon.stub()
        };
    }

    query<T>(query): Promise<Array<T>> {
        return this.stubs.query(query);
    }
}