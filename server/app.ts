import { AppServerFactory } from './src/server/server_factory';
import { ConsoleLogger } from './src/logger/console_logger';
import { FsConfigReader } from './src/configureation/fs_config_reader';
import { argv } from 'optimist';
import * as fs from 'fs';
import { ServerRouterFactory } from './src/server/router';
import { ProxyRouter } from './src/server/routes/proxy_router';
import { QuestionnairesProvider } from './src/dao/questionnaires_provider';
import { NextQuestionsProviderSql } from './src/dao/sql/next_question_provider';
import { AnswersDao } from './src/dao/sql/answer_dao';
import { QuestionsDao } from './src/dao/sql/questions_dao';
import { SqlightDbFactory } from './src/sqlight/sqlight_client';
import { NotFoundRoute } from './src/server/routes/not_found_route';
import { QuestionnairesDao } from './src/dao/sql/questionnaires_dao';
import { QuestionnaireRoute } from './src/server/routes/questionnaire/questionnaire_route';
import { QuestionRoute } from './src/server/routes/questionnaire/question_route';
import { AnswerRouter } from './src/server/routes/questionnaire/answer_route';
import { NextQuestionRoute } from './src/server/routes/questionnaire/next_question_route';

namespace Config {
    let path = argv.configPath || './config.json';
    export const provider = new FsConfigReader(path, fs);
}

export const logger = new ConsoleLogger(console);

logger.info({ description: 'Application config', config: Config.provider.getConfig(), requestId: 'N/A' });

namespace Sql {
    export namespace Questions {
        const pathProvider = () => Config.provider.getConfig().db.questions.path;
        export const client = new SqlightDbFactory(pathProvider).getProvider();
    }
}

namespace Dao {
    export namespace Questionnaire {
        const questions = new QuestionsDao(logger, Sql.Questions.client);
        export const answers = new AnswersDao(logger, Sql.Questions.client);
        export const questionnaires = new QuestionnairesDao(logger, Sql.Questions.client);
        const nextQuestionProvider = new NextQuestionsProviderSql(logger, Sql.Questions.client);
        export const provider = new QuestionnairesProvider(logger, questions, answers, nextQuestionProvider, questionnaires);
    }
}

namespace Server {
    namespace Routes {
        export const proxy = new ProxyRouter(logger);
        export const notFound = new NotFoundRoute();
        export namespace Questionnaire {
            export const questionnaireRoute = new QuestionnaireRoute(Dao.Questionnaire.provider);
            export const questionRoute = new QuestionRoute(Dao.Questionnaire.provider);
            export const answersRoute = new AnswerRouter(Dao.Questionnaire.provider);
            export const nextQuestion = new NextQuestionRoute(Dao.Questionnaire.provider);
        }
    }
    
    let router = new ServerRouterFactory(Routes.proxy,
        Routes.notFound, 
        Routes.Questionnaire.questionnaireRoute,
        Routes.Questionnaire.questionRoute,
        Routes.Questionnaire.answersRoute,
        Routes.Questionnaire.nextQuestion);
    let factory = new AppServerFactory(Config.provider, router, logger);
    export const app = factory.create();
}

Server.app.start()
      .then(() => logger.info({ description: 'Server is started', requestId: 'N/A' }))
      .catch(err => logger.emergency({ description: 'Server cannot be started', err, requestId: 'N/A' }));