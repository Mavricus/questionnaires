type Parameter = string | number;

export const buildHomeUrl = (): string => '/';

export const buildConfigurationUrl = (): string => '/configuration';

export const buildQuestionnaireCreateUrl = (): string => '/questionnaire/create';
export const buildQuestionnaireEditUrl = (questionnaireId: Parameter): string => `/questionnaire/${questionnaireId}/edit`;

export const buildQuestionCreateUrl = (questionnaireId: Parameter): string => `/questionnaire/${questionnaireId}/question/create`;
export const buildQuestionEditUrl = (questionnaireId: Parameter, questionId: Parameter): string => `/questionnaire/${questionnaireId}/question/${questionId}/edit`;

export const buildAnswerCreateUrl = (questionnaireId: Parameter, questionId: Parameter): string => `/questionnaire/${questionnaireId}/question/${questionId}/answer/create`;
export const buildAnswerEditUrl = (questionnaireId: Parameter, questionId: Parameter, answerId: Parameter): string => `/questionnaire/${questionnaireId}/question/${questionId}/answer/${answerId}/edit`;

