import * as url from 'url';

type Parameter = string | number;
let { protocol, hostname } = url.parse(window.location.href);
const Server = url.format({ protocol, hostname, port: 888 });

export const buildGetAllQuestionnairesUrl = () => `${Server}/questionnaires`;

export const buildGetQuestionnaireUrl = (questionnaireId: Parameter) => `${Server}/questionnaire/${questionnaireId}`;
export const buildPathQuestionnaireUrl = (questionnaireId: Parameter) => `${Server}/questionnaire/${questionnaireId}`;
export const buildDeleteQuestionnaireUrl = (questionnaireId: Parameter) => `${Server}/questionnaire/${questionnaireId}`;
export const buildGetQuestionnaireQuestionsUrl = (questionnaireId: Parameter) => `${Server}/questionnaire/${questionnaireId}/questions`;
export const buildPostQuestionnaireUrl = () => `${Server}/questionnaire`;

export const buildGetQuestionUrl = (questionId: Parameter) => `${Server}/question/${questionId}`;
export const buildPatchQuestionUrl = (questionId: Parameter) => `${Server}/question/${questionId}`;
export const buildPostQuestionUrl = () => `${Server}/question`;
export const buildDeleteQuestionUrl = (questionId: Parameter) => `${Server}/question/${questionId}`;

export const buildGetAnswerUrl = (answerId: Parameter) => `${Server}/answer/${answerId}`;
export const buildDeleteAnswerUrl = (answerId: Parameter) => `${Server}/answer/${answerId}`;
export const buildPostAnswerUrl = () => `${Server}/answer`;
export const buildPatchAnswerUrl = (answerId: Parameter) => `${Server}/answer/${answerId}`;

export const buildDeleteNextQuestionUrl = (answerId: Parameter, questionId: Parameter) => `${Server}/nextQuestion/${answerId}/${questionId}`;
export const buildPostNextQuestionUrl = () => `${Server}/nextQuestion`;
