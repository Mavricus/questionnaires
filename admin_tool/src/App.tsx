import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.css";

import { NavigationBar } from './components/navbar.component';
import { Configuration } from './components/configuration.component';
import { QuestionEdit } from './components/question/question_edit.component';
import { QuestionnaireEdit } from './components/questionnaire/questionnaire_edit.component';
import { QuestionnairesList } from './components/questionnaire/questionnaires_list.component';
import { AnswerEdit } from './components/answer/answer_edit.component';
import { QuestionnaireCreate } from './components/questionnaire/questionnaire_create.component';
import * as urlBuilders from './utils/urls_builder';
import { QuestionCreate } from './components/question/question_create.component';
import { AnswerCreate } from './components/answer/answer_create.component';

const App: React.FC = () => {
  return (
    <Router>
        <NavigationBar/>
        <br/>
        <Route path={urlBuilders.buildHomeUrl()} exact component={QuestionnairesList} />
        <Route path={urlBuilders.buildConfigurationUrl()} exact component={Configuration} />

        <Route path={urlBuilders.buildQuestionnaireCreateUrl()} exact component={QuestionnaireCreate} />
        <Route path={urlBuilders.buildQuestionnaireEditUrl(':questionnaireId')} exact component={QuestionnaireEdit} />

        <Route path={urlBuilders.buildQuestionEditUrl(':questionnaireId', ':questionId')} exact component={QuestionEdit} />
        <Route path={urlBuilders.buildQuestionCreateUrl(':questionnaireId')} exact component={QuestionCreate} />

        <Route path={urlBuilders.buildAnswerEditUrl(':questionnaireId', ':questionId', ':answerId')} exact component={AnswerEdit} />
        <Route path={urlBuilders.buildAnswerCreateUrl(':questionnaireId', ':questionId')} exact component={AnswerCreate} />
    </Router>
  );
};

export default App;
