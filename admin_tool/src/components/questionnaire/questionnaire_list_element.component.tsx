import React, { FunctionComponent } from 'react';
import { MdDelete, MdEdit } from 'react-icons/md';
import { Link } from 'react-router-dom';

interface Props {
    id: string;
    description: string;
    onEdit: (e: React.MouseEvent<HTMLButtonElement>) => void;
    onDelete: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

export const QuestionnaireListElement: FunctionComponent<Props> =  (props: Props) => {
    return <tr>
        <td style={{ verticalAlign: "middle" }}>
            <Link to='#' onClick={(e: any) => props.onEdit(e)}>{props.description}</Link>
        </td>
        <td>
            <button className="btn btn-dark btn-group" type="button" onClick={e => props.onEdit(e)}><MdEdit/></button>
            <button className="btn btn-danger btn-group" type="button" onClick={e => props.onDelete(e)}><MdDelete/></button>
        </td>
    </tr>;
};