import React, { Component, FormEvent } from 'react';
import { RouteComponentProps } from 'react-router';
import { Button, Form } from 'react-bootstrap';
import { QuestionnaireDetails } from './questionnaire_details.component';
import { buildQuestionnaireEditUrl } from '../../utils/urls_builder';
import axios from'axios';
import { buildPostQuestionnaireUrl } from '../../utils/request_urls_builder';

interface PathProps {
}

interface ServerResponse {
    questionnaire: {
        id: string;
        description: string;
    };
}

interface Props extends RouteComponentProps<PathProps> {
}

interface State {
    description: string;
}

export class QuestionnaireCreate extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            description: ''
        };
    }

    render() {
        return <div className="container">
            <h3>Create new Questionnaire</h3>
            <Form onSubmit={(e: FormEvent<HTMLFormElement>) => this.onSubmit(e)}>
                <QuestionnaireDetails
                    id=""
                    description={this.state.description}
                    onDescriptionChange={(description) => this.setState({ description })}/>
                <Button className="col-sm-2" variant="primary" type="submit">Create</Button>
                <Button className="col-sm-2 float-right" variant="danger" onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.onCancel(e)}>Cancel</Button>
            </Form>
        </div>;
    }

    onCancel(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        e.preventDefault();

        this.props.history.goBack();
    }

    onSubmit(e: React.FormEvent<HTMLFormElement>): void {
        e.preventDefault();

        axios.post<ServerResponse>(buildPostQuestionnaireUrl(), this.state)
             .then(({ status, data }) => {
                 if (status !== 201) {
                     throw new Error('Cannot create the questionnaire');
                 }
                 return data.questionnaire;
             })
             .then(({id}) => this.props.history.push(buildQuestionnaireEditUrl(id)))
    }
}