import React, { Component } from 'react';
import { Button, Table } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { QuestionnaireListElement } from './questionnaire_list_element.component';
import axios from 'axios';
import { buildDeleteQuestionnaireUrl, buildGetAllQuestionnairesUrl } from '../../utils/request_urls_builder';
import { Loading } from '../loadind.component';
import promiseRetry from 'promise-retry';

interface Props extends RouteComponentProps<{}> {
}

interface Questionnaire {
    id: string;
    description: string;
}

interface State {
    questionnaires: Array<Questionnaire>;
    isLoaded: boolean;
}

interface ServerResponse {
    "questionnaires": [
        {
            "id": "1",
            "description": "Rhetorical Questions"
        }
    ]
}

export class QuestionnairesList extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            questionnaires: [],
            isLoaded: false
        }
    }

    componentDidMount(): void {
        promiseRetry((retry) => axios.get<ServerResponse>(buildGetAllQuestionnairesUrl()).catch(retry), { forever: true })
            .then(({ data, status }) => {
                 if (status !== 200) {
                     throw new Error('Cannot get questionnaires list');
                 }
                 return data.questionnaires;
             })
             .then(questionnaires => this.setState({ questionnaires, isLoaded: true }));
    }

    render() {
        return <div className="container">
            <Loading inProgress={!this.state.isLoaded}/>
            <div hidden={!this.state.isLoaded}>
                <h3>Questionnaires</h3>
                <div className="row">
                    <Table bordered={true} striped={true}>
                        <thead>
                            <tr>
                                <th scope="col">Description</th>
                                <th style={{width: "110px"}} scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            { this.state.questionnaires.map(({ id, description }) =>
                                <QuestionnaireListElement
                                    key={id}
                                    description={description}
                                    id={id}
                                    onEdit={e => this.onQuestionnaireEdit(e, id)}
                                    onDelete={e => this.onQuestionnaireDelete(e, id)}/>) }
                        </tbody>
                    </Table>
                    <Button className="col-sm-2" type="button" variant="primary" onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.onCreateClick(e)}>Add</Button>
                </div>
            </div>
        </div>
    }

    private onCreateClick(e: React.MouseEvent<HTMLButtonElement>): void {
        this.props.history.push(`/questionnaire/create`);
    }

    private onQuestionnaireEdit(e: React.MouseEvent<HTMLButtonElement>, id: string) {
        e.preventDefault();
        this.props.history.push(`/questionnaire/${id}/edit`);
    }

    private onQuestionnaireDelete(e: React.MouseEvent<HTMLButtonElement>, id: string) {
        e.preventDefault();
        axios.delete(buildDeleteQuestionnaireUrl(id))
             .then(({ status }) => {
                 if (status !== 204){
                     throw new Error('Cannot delete the questionnaire');
                 }
             }).then(() => this.setState({ questionnaires: this.state.questionnaires.filter(q => q.id !== id) }));
    }
}