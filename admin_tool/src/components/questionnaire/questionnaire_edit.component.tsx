import React, { Component, FormEvent } from 'react';
import { RouteComponentProps } from 'react-router';
import { QuestionnaireDetails } from './questionnaire_details.component';
import { Button, Form, FormGroup } from 'react-bootstrap';
import { buildHomeUrl, buildQuestionCreateUrl, buildQuestionEditUrl } from '../../utils/urls_builder';
import { QuestionsList } from '../question/questions_list.component';
import axios from 'axios';
import { buildDeleteQuestionUrl, buildGetQuestionnaireUrl, buildPathQuestionnaireUrl } from '../../utils/request_urls_builder';
import { Loading } from '../loadind.component';
import promiseRetry from 'promise-retry';


interface PathData {
    questionnaireId: string;
}

interface Props extends RouteComponentProps<PathData> {
}

interface State {
    id: string;
    description: string;
    questions: Array<{ id: string; text: string; }>;
    isUpdated: boolean;
    isLoaded: boolean;
}

interface ServerResponse {
    questionnaire: {
        id: string;
        description: string;
        questions: Array<{ id: string; text: string; }>;
    };
}

export class QuestionnaireEdit extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state ={
            id: props.match.params.questionnaireId,
            description: '',
            questions: [],
            isUpdated: false,
            isLoaded: false
        }
    }

    componentDidMount(): void {
        promiseRetry(retry => axios.get<ServerResponse>(buildGetQuestionnaireUrl(this.state.id)).catch(retry), { forever: true })
             .then(({ data, status }) => {
                 if (status !== 200) {
                     throw new Error('Cannot get questionnaire details');
                 }
                 return data.questionnaire;
             })
             .then(({ description, questions }) => this.setState({ description, questions, isLoaded: true }));
    }

    render() {
        return <div className="container">
            <Loading inProgress={!this.state.isLoaded}/>
            <div hidden={!this.state.isLoaded}>
            <h3>Questionnaire <span className="text-monospace">{this.state.id }</span></h3>
            <Form onSubmit={(e: FormEvent<HTMLFormElement>) => this.onSubmit(e)}>
                <FormGroup>
                    <QuestionnaireDetails
                        id={this.state.id}
                        description={this.state.description}
                        onDescriptionChange={description => this.setState({ description, isUpdated: true })}
                    />
                </FormGroup>
                <FormGroup>
                    <Button className="col-sm-2" variant="primary" type="submit" disabled={!this.state.isUpdated}>Save</Button>
                    <Button className="col-sm-2 float-right" variant="danger" type="button" onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.onCancel(e)}>Cancel</Button>
                </FormGroup>
            </Form>
            <h5>Questions:</h5>
            <QuestionsList
                questions={this.state.questions}
                onDelete={(e, id) => this.onQuestionDelete(e, id)}
                onEdit={(e, id) => this.onQuestionEdit(e, id)}/>
            <Button
                className="col-sm-2"
                variant="primary"
                type="button"
                onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.onAddQuestion(e)}>Add</Button>
            </div>
        </div>
    }

    private onAddQuestion(e: React.MouseEvent<HTMLButtonElement>): void {
        e.preventDefault();

        this.props.history.push(buildQuestionCreateUrl(this.state.id));
    }

    private onSubmit(e: React.FormEvent<HTMLFormElement>): void {
        e.preventDefault();
        axios.patch(buildPathQuestionnaireUrl(this.state.id), this.state)
             .then(({ status }) => {
                 if (status !== 204) {
                     throw new Error('Cannot update the question');
                 }
             })
             .then(() => this.setState({ isUpdated: false }));
    }

    private onCancel(e: React.MouseEvent<HTMLButtonElement>): void {
        e.preventDefault();
        
        this.props.history.push(buildHomeUrl());
    }

    private onQuestionEdit(e: React.MouseEvent<HTMLButtonElement>, id: string) {
        e.preventDefault();

        this.props.history.push(buildQuestionEditUrl(this.state.id, id));
    }

    private onQuestionDelete(e: React.MouseEvent<HTMLButtonElement>, id: string) {
        e.preventDefault();

        axios.delete(buildDeleteQuestionUrl(id))
             .then(({ status }) => {
                 if (status !== 204) {
                     throw new Error('Cannot delete question');
                 }
             }).then(() => this.setState({ questions: this.state.questions.filter(q => q.id !== id) }));
    }
}