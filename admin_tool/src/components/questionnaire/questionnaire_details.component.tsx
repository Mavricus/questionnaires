import React, { FunctionComponent } from 'react';
import { Form, FormGroup } from 'react-bootstrap';

interface Props {
    id: string;
    description: string;
    onDescriptionChange: (description: string) => void;
}

export const QuestionnaireDetails: FunctionComponent<Props> = (props: Props)  => {
    return <FormGroup>
        <Form.Label>Description</Form.Label>
        <Form.Control
            id={`description-${props.id}`}
            required={true}
            type="text"
            placeholder="Description"
            defaultValue={props.description}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => props.onDescriptionChange(e.target.value)}/>
    </FormGroup>
}