import React, { FunctionComponent } from 'react';
import { Spinner } from 'react-bootstrap';

interface Props {
    inProgress: boolean;
}

export let Loading: FunctionComponent<Props> = (props: Props) => {
    return <div className="text-center" hidden={!props.inProgress}>
        <Spinner animation="grow" role="status">
            <span className="sr-only">Loading...</span>
        </Spinner>
    </div>;
};