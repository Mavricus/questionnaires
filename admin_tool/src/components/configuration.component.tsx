import React, { Component } from 'react';

interface ConfigurationState {
    questionTypes: Array<{ name: string; id: string }>;
    answerTypes: Array<{ name: string; id: string }>;
}

export class Configuration extends Component<{}, ConfigurationState> {
    constructor(props: {}) {
        super(props);

        this.state = {
            answerTypes: [],
            questionTypes: []
        }
    }
    
    render() {
        return <p>THis is Configuration page</p>
    }
}