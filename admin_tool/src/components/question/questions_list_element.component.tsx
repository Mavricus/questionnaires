import React, { FunctionComponent } from 'react';
import { InputGroup } from 'react-bootstrap';
import { MdDelete, MdEdit } from 'react-icons/md';
import { Link } from 'react-router-dom';

interface Props {
    id: string;
    text: string;
    onEdit: (e: React.MouseEvent<HTMLButtonElement>, id: string) => void;
    onDelete: (e: React.MouseEvent<HTMLButtonElement>, id: string) => void;
}

interface State {
    id: string;
    text: string;
}

export const QuestionsListElement: FunctionComponent<Props> = ({ id, text, onDelete, onEdit }) => {
    return <InputGroup className="mb-3">
        <Link to='#' type="text" className="form-control" onClick={(e: any) => onEdit(e, id)}>{text}</Link>
        <div className="input-group-append">
            <button className="btn btn-dark" type="button" onClick={e => onEdit(e, id)}><MdEdit/></button>
            <button className="btn btn-danger" type="button" onClick={e => onDelete(e, id)}><MdDelete/></button>
        </div>
    </InputGroup>;
};