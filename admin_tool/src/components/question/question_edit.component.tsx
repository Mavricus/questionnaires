import React, { Component, FormEvent } from 'react';
import { RouteComponentProps } from 'react-router';
import { Button, FormGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import { AnswersList } from '../answer/answers_list.component';
import axios from 'axios';
import { buildGetQuestionUrl, buildPatchQuestionUrl } from '../../utils/request_urls_builder';
import { buildAnswerCreateUrl, buildAnswerEditUrl, buildQuestionnaireEditUrl } from '../../utils/urls_builder';
import { QuestionDetails } from './question_details.component';
import promiseRetry from 'promise-retry';
import { Loading } from '../loadind.component';

interface State {
    id: string;
    text: string;
    isInitial: boolean;
    answers: Array<{ text: string, id: string }>;
    questionnaireId: string;
    isUpdated: boolean;
    isLoaded: boolean;
}

interface PathParams {
    questionId: string;
    questionnaireId: string;
}

interface Props extends RouteComponentProps<PathParams> {
}

interface ServerResponse {
    question: {
        id: string;
        text: string;
        isInitial: boolean;
        answers: Array<{
            id: string;
            text: string;
        }>;
    };
}

export class QuestionEdit extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            id: props.match.params.questionId,
            text: '',
            isInitial: false,
            questionnaireId: props.match.params.questionnaireId,
            answers: [],
            isUpdated: false,
            isLoaded: false
        }
    }

    componentDidMount(): void {
        promiseRetry(retry => axios.get<ServerResponse>(buildGetQuestionUrl(this.state.id)).catch(retry), { forever: true })
             .then(({ data, status }) => {
                 if (status !== 200)  {
                     throw new Error('Cannot retrieve question information');
                 }
                 return data.question;
             })
             .then(({ text, answers, isInitial }) => {
                 let newState =  { text, answers, isInitial, isLoaded: true };
                 this.setState(newState);
             })
             .catch(err => {
                 console.log(err);
             });
    }

    render() {
        return <div className="container">
            <Loading inProgress={ !this.state.isLoaded }/>
            <div hidden={ !this.state.isLoaded }>
                <h3>Question <span className="text-monospace">{this.state.id }</span></h3>
                <Form onSubmit={(e: FormEvent<HTMLFormElement>) => this.onSubmit(e)}>
                    <FormGroup>
                        <QuestionDetails
                            id={this.state.id}
                            text={this.state.text}
                            isInitial={this.state.isInitial}
                            onTextChange={text => this.setState({ text, isUpdated: true })}
                            onIsInitialChange={isInitial => this.setState({ isInitial, isUpdated: true })}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Button className="col-sm-2" type="submit" variant="primary" disabled={!this.state.isUpdated}>Save</Button>
                        <Button className="col-sm-2 float-right" variant="danger" type="button" onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.onCancel(e)}>Cancel</Button>
                    </FormGroup>
                </Form>
                <h5>Answers:</h5>
                <AnswersList
                    answers={this.state.answers}
                    onEdit={id => this.onAnswerEdit(id)}
                    onDelete={id => this.onAnswerDelete(id)}/>
                <Button className="col-sm-2" variant="primary" type="button" onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.onAddAnswer(e)}>Add</Button>
            </div>
        </div>
    }

    onAddAnswer(e: React.MouseEvent<HTMLButtonElement>): void {
        e.preventDefault();
        
        this.props.history.push(buildAnswerCreateUrl(this.state.questionnaireId, this.state.id));
    }

    onCancel(e: React.MouseEvent<HTMLButtonElement>): void {
        e.preventDefault();

        this.props.history.push(buildQuestionnaireEditUrl(this.state.questionnaireId));
    }

    private onSubmit(e: FormEvent<HTMLFormElement>) {
        e.preventDefault();

        axios.patch(buildPatchQuestionUrl(this.state.id), this.state)
             .then(({ status }) => {
                 if (status !== 204) {
                     throw new Error('Cannot update the question')
                 }
             })
             .then(() => this.setState({ isUpdated: false }));
    }

    private onAnswerEdit(id: string) {
        console.log(`Answer ${id} has been edited`);
        this.props.history.push(buildAnswerEditUrl(this.state.questionnaireId, this.state.id, id));
    }

    private onAnswerDelete(id: string) {
        const newState = {
            answers: this.state.answers.filter(a => a.id !== id)
        };
        this.setState(newState);
        console.log(`Answer ${id} has been deleted`);
        console.log(this.state);
    }
}