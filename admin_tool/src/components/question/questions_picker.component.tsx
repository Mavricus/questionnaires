import React, { Component } from 'react';
import { Button, Modal, Table } from 'react-bootstrap';
import { Loading } from '../loadind.component';
import promiseRetry from 'promise-retry'
import axios from 'axios';
import { buildGetQuestionnaireQuestionsUrl } from '../../utils/request_urls_builder';
import { Form } from 'react-bootstrap';

interface Props {
    show: boolean;
    questionnaireId: string;
    selectedQuestions: Array<string>;
    onHide: () => void;
    onSelect: (list: Array<{ id: string; text: string; }>) => void;
}

interface State {
    isLoaded: boolean;
    isSelected: boolean;
    questions: Array<{ id: string; text: string; }>;
    selectedQuestions: Array<string>;
}

interface ServerResponse {
    questions: Array<{ id: string; text: string; }>;
}

export class QuestionsPicker extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            isLoaded: false,
            isSelected: false,
            questions: [],
            selectedQuestions: []
        }
    }

    componentDidMount() {
        promiseRetry(retry => axios.get<ServerResponse>(buildGetQuestionnaireQuestionsUrl(this.props.questionnaireId)).catch(retry), { forever: true })
            .then(({ status, data }) => {
                if (status !== 200) {
                    throw new Error('Cannot retrieve questions list');
                }
                return data.questions;
            })
            .then(questions => this.setState({ questions, isLoaded: true }));
    }

    render() {
        return <Modal
            show={this.props.show}
            onHide={this.props.onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            dialogClassName="modal-90w"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">Questions</Modal.Title>
            </Modal.Header>
            <Loading inProgress={!this.state.isLoaded}/>
            <Modal.Body hidden={!this.state.isLoaded}>
                <Table borderless={true}>
                    <tbody>
                        {this.getQuestionsMap().map(q => {
                            return <tr key={q.id}>
                                <td style={{width: "40px"}}>
                                    <Form.Check
                                        type="checkbox"
                                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => this.selectQuestion(e, q.id)}
                                    />
                                </td>
                                <td>{q.text}</td>
                            </tr>
                        })}
                    </tbody>
                </Table>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={this.props.onHide}>Close</Button>
                <Button
                    variant="primary"
                    disabled={!this.state.isSelected} 
                    onClick={() => this.props.onSelect(this.state.questions.filter(q => this.state.selectedQuestions.some(sq => sq === q.id)))}>Select</Button>
            </Modal.Footer>
        </Modal>
    }

    getQuestionsMap(): Array<{ id: string; text: string; }> {
        return this.state.questions.filter(q => !this.props.selectedQuestions.some(sq => sq === q.id));
    }

    private selectQuestion(e: React.ChangeEvent<HTMLInputElement>, id: string) {
        let checked = e.target.checked;
        let select = this.state.selectedQuestions.slice();
        if (checked) {
            select.push(id);
        } else {
            select = select.filter(q => q !== id);
        }

        this.setState({ selectedQuestions: select, isSelected: !!select.length });
    }
}