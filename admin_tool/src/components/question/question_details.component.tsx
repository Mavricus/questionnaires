import React, { FunctionComponent } from 'react';
import { Form, FormGroup } from 'react-bootstrap';

interface Props {
    id: string;
    text: string;
    isInitial: boolean;
    onTextChange: (text: string) => void;
    onIsInitialChange: (value: boolean) => void;
}

export const QuestionDetails: FunctionComponent<Props> = props => {
    return <FormGroup>
        <Form.Group>
            <Form.Label id="inputGroupPrepend">Text</Form.Label>
            <Form.Control
                id={`text-${props.id}`}
                required={true}
                type="text"
                placeholder="Question text"
                defaultValue={props.text}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => props.onTextChange(e.target.value)}
            />
        </Form.Group>
        <Form.Group>
            <Form.Check
                label='Is initial question'
                checked={props.isInitial}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => props.onIsInitialChange(e.target.checked)}
            />
        </Form.Group>
    </FormGroup>
};