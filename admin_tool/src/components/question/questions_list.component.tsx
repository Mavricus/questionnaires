import React, { FunctionComponent } from 'react';
import { Form } from 'react-bootstrap';
import { QuestionsListElement } from './questions_list_element.component';

interface Question {
    id: string;
    text: string;
}

interface Props {
    questions: Array<Question>;
    onEdit: (e: React.MouseEvent<HTMLButtonElement>, id: string) => void;
    onDelete: (e: React.MouseEvent<HTMLButtonElement>, id: string) => void;
}

export const QuestionsList: FunctionComponent<Props> = ({ questions, onDelete, onEdit }: Props) => {
    return <Form>
        {questions.map(({ id, text }) =>  <QuestionsListElement key={id} text={text} id={id} onEdit={onEdit} onDelete={onDelete}/>) }
    </Form>;
};