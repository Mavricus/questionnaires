import React, { Component, FormEvent } from 'react';
import { RouteComponentProps } from 'react-router';
import { Button, Form } from 'react-bootstrap';
import { QuestionDetails } from './question_details.component';
import { buildQuestionEditUrl, buildQuestionnaireEditUrl } from '../../utils/urls_builder';
import { buildPostQuestionUrl } from '../../utils/request_urls_builder';
import axios from 'axios';

interface ServerResponse {
    question: {
        id: string;
        text: string;
        questionnaireId: string;
    };
}

interface UrlParams {
    questionnaireId: string;
}

interface Props extends RouteComponentProps<UrlParams> {
    questionnaireId: string;
}

interface State {
    text: string;
    questionnaireId: string;
    isInitial: boolean;
}

export class QuestionCreate extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            text: '',
            questionnaireId: props.match.params.questionnaireId,
            isInitial: false
        };
    }
    
    render() {
        return <div className="container">
            <h3>Create new Question</h3>
            <Form onSubmit={(e: FormEvent<HTMLFormElement>) => this.onSubmit(e)}>
                <QuestionDetails
                    id=""
                    text={this.state.text}
                    isInitial={this.state.isInitial}
                    onTextChange={text => this.setState({ text })}
                    onIsInitialChange={isInitial => this.setState({ isInitial })}
                />
                <Button className="col-sm-2" variant="primary" type="submit">Create</Button>
                <Button className="col-sm-2 float-right" variant="danger" onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.onCancel(e)}>Cancel</Button>
            </Form>
        </div>;
    }

    private onSubmit(e: React.FormEvent<HTMLFormElement>): void {
        e.preventDefault();

        axios.post<ServerResponse>(buildPostQuestionUrl(), this.state)
             .then(({ status, data }) => {
                 if (status !== 201) {
                     throw new Error('Cannot create the question');
                 }
                 return data.question;
             }).then(({ id }) => this.props.history.push(buildQuestionEditUrl(this.state.questionnaireId, id)));
    }

    private onCancel(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();

        this.props.history.push(buildQuestionnaireEditUrl(this.state.questionnaireId));
    }
}