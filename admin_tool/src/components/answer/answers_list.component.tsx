import React, { FunctionComponent } from 'react';
import { AnswersListElement } from './answers_list_element.component';
import { Form } from 'react-bootstrap';

interface Props {
    answers: Array<{ id: string; text: string; }>;
    onEdit: (id: string) => void;
    onDelete: (id: string) => void;
}

export const AnswersList: FunctionComponent<Props> = ({ answers, onDelete, onEdit }: Props) => {
    return <Form>
        { answers.map(({ id, text }) =>  <AnswersListElement key={id} text={text} id={id} onEdit={onEdit} onDelete={onDelete}/>) }
    </Form>;
};