import React, { Component, FormEvent } from 'react';
import { RouteComponentProps } from 'react-router';
import { Button, Form } from 'react-bootstrap';
import {
    buildDeleteNextQuestionUrl, buildGetAnswerUrl, buildPatchAnswerUrl, buildPostNextQuestionUrl
} from '../../utils/request_urls_builder';
import { AnswerDetails } from './answer_details.component';
import { buildQuestionEditUrl } from '../../utils/urls_builder';
import { FormGroup } from 'react-bootstrap';
import { QuestionsList } from '../question/questions_list.component';
import axios from 'axios';
import promiseRetry from 'promise-retry'
import { Loading } from '../loadind.component';
import { QuestionsPicker } from '../question/questions_picker.component';

interface PathParams {
    answerId: string;
    questionId: string;
    questionnaireId: string;
}

interface Props extends RouteComponentProps<PathParams> {
}

interface State {
    text: string;
    isUpdated: boolean;
    isLoaded: boolean;
    isQuestionsPickerLoaded: boolean;
    showQuestionsPicker: boolean;
    areQuestionsLoaded: boolean;
    nextQuestions: Array<{ id: string; text: string; }>;
}

interface ServerResponse {
    answer: {
        id: string;
        text: string;
        questionId: string;
        nextQuestions: Array<{ id: string; text: string; }>;
    };
}

export class AnswerEdit extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            text: '',
            isUpdated: false,
            isLoaded: false,
            showQuestionsPicker: false,
            isQuestionsPickerLoaded: false,
            areQuestionsLoaded: false,
            nextQuestions: []
        }
    }

    componentDidMount() {
        promiseRetry(retry => axios.get<ServerResponse>(buildGetAnswerUrl(this.props.match.params.answerId)).catch(retry), { forever: true })
            .then(({ data, status }) => {
                if (status !== 200) {
                    throw new Error('Cannot retrieve answer information');
                }
                return data.answer;
            }).then(({ text, nextQuestions }) => this.setState({ text, nextQuestions, isLoaded: true }));
    }

    render() {
        return <div className="container">
            <Loading inProgress={!this.state.isLoaded}/>
            <div hidden={!this.state.isLoaded}>
                <h3>Answer <span className="text-monospace">{this.props.match.params.answerId}</span></h3>
                <Form onSubmit={(e: FormEvent<HTMLFormElement>) => this.onSubmit(e)}>
                    <FormGroup>
                        <AnswerDetails
                            id={this.props.match.params.answerId}
                            text={this.state.text}
                            onTextChange={text => this.setState({ text, isUpdated: true })}/>
                    </FormGroup>
                    <FormGroup>
                        <Button className="col-sm-2" type="submit" variant="primary" disabled={!this.state.isUpdated}>Save</Button>
                        <Button className="col-sm-2 float-right" type="button" variant="danger" onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.onCancel(e)}>Cancel</Button>
                    </FormGroup>
                </Form>
                <h5>Next questions:</h5>
                <QuestionsList
                    questions={ this.state.nextQuestions }
                    onEdit={(e, id) => this.onQuestionEdit(e, id)}
                    onDelete={(e, id) => this.onQuestionDelete(e, id)}
                />
                <Button className="col-sm-2" variant="primary" type="button" onClick={() => this.setState({ showQuestionsPicker: true })}>Add</Button>
            </div>
            <QuestionsPicker
                show={this.state.showQuestionsPicker}
                selectedQuestions={this.getSelectedQuestions()}
                questionnaireId={this.props.match.params.questionnaireId}
                onHide={() => this.setState({ showQuestionsPicker: false })}
                onSelect={(questions) => this.addNextQuestions(questions)}
            />
        </div>;
    }

    private getSelectedQuestions(): Array<string> {
        let selected = this.state.nextQuestions.map(q => q.id);
        selected.push(this.props.match.params.questionId);
        return selected;
    }

    private addNextQuestions(questions: Array<{ id: string; text: string; }>) {
        Promise.all(questions.map(({ id }) => {
            const data = { answerId: this.props.match.params.answerId, questionId: id };
            return axios.post(buildPostNextQuestionUrl(), data);
        })).then(list => {
            if (list.some(({ status }) => status !== 201)) {
                throw new Error('Cannot add next questions');
            }
       }).then(() => {
           const nextQuestions = this.state.nextQuestions.concat(questions);
           this.setState({ nextQuestions, showQuestionsPicker: false });
       });
    }

    private onQuestionDelete(e: React.MouseEvent<HTMLButtonElement, MouseEvent>, id: string) {
        e.preventDefault();

        axios.delete<void>(buildDeleteNextQuestionUrl(this.props.match.params.answerId, id))
             .then(({ status }) => {
                 if (status !== 204) {
                     throw new Error('Cannot delete Next Question');
                 }
             })
             .then(() => {
                 const nextQuestions = this.state.nextQuestions.filter(q => q.id !== id);
                 this.setState({ nextQuestions });
             });
    }

    private onQuestionEdit(e: React.MouseEvent<HTMLButtonElement, MouseEvent>, id: string): void {
        e.preventDefault();

        this.props.history.push(buildQuestionEditUrl(this.props.match.params.questionnaireId, id));
    }

    private onCancel(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();

        this.props.history.push(buildQuestionEditUrl(this.props.match.params.questionnaireId, this.props.match.params.questionId));
    }

    private onSubmit(e: FormEvent<HTMLFormElement>) {
        e.preventDefault();
        axios.patch(buildPatchAnswerUrl(this.props.match.params.answerId), this.state)
             .then(({ status }) => {
                 if (status !== 204) {
                     throw new Error('Cannot update question');
                 }
             })
             .then(() => this.props.history.push(buildQuestionEditUrl(this.props.match.params.questionnaireId, this.props.match.params.questionId)));
    }
}