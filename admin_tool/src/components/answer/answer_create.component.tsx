import React, { Component, FormEvent } from 'react';
import axios from 'axios';
import { Button, Form } from 'react-bootstrap';
import { AnswerDetails } from './answer_details.component';
import { buildQuestionEditUrl } from '../../utils/urls_builder';
import { RouteComponentProps } from 'react-router';
import { buildPostAnswerUrl } from '../../utils/request_urls_builder';

interface ServerResponse {
    answer: {
        id: string;
        text: string;
        questionId: string;
    }
}

interface PathParams {
    questionnaireId: string;
    questionId: string;
}

interface Props extends RouteComponentProps<PathParams> {
}

interface State {
    text: string;
    questionId: string;
    questionnaireId: string;
}

export class AnswerCreate extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            text: '',
            questionId: props.match.params.questionId,
            questionnaireId: props.match.params.questionnaireId
        }
    }

    render() {
        return <div className="container">
            <h3>Create Answer</h3>
            <Form onSubmit={(e: FormEvent<HTMLFormElement>) => this.onSubmit(e)}>
                <AnswerDetails
                    id=""
                    text={this.state.text}
                    onTextChange={text => this.setState({ text })}/>
                <Button className="col-sm-2" type="submit" variant="primary">Save</Button>
                <Button className="col-sm-2 float-right" type="button" variant="danger" onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.onCancel(e)}>Cancel</Button>
            </Form>
        </div>;
    }

    private onCancel(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();

        this.props.history.push(buildQuestionEditUrl(this.state.questionnaireId, this.state.questionId));
    }

    private onSubmit(e: FormEvent<HTMLFormElement>) {
        e.preventDefault();

        axios.post<ServerResponse>(buildPostAnswerUrl(), this.state)
             .then(({ data, status}) => {
                 if (status !== 201) {
                     throw new Error('Cannot add answer');
                 }
                 return data.answer;
             })
             .then(() => this.props.history.push(buildQuestionEditUrl(this.state.questionnaireId, this.state.questionId)));
    }
}