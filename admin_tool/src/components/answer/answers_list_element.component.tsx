import React, { Component, FormEvent } from 'react';
import { InputGroup } from 'react-bootstrap';
import { MdDelete, MdEdit } from 'react-icons/md';
import axios from 'axios';
import { buildDeleteAnswerUrl } from '../../utils/request_urls_builder';
import { Link } from 'react-router-dom';

export interface Answer {
    id: string;
    text: string;
}

interface Props {
    id: string;
    text: string;
    onEdit: (id: string) => void;
    onDelete: (id: string) => void;
}

interface State extends Answer {
}

export class AnswersListElement extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            id: props.id,
            text: props.text
        }
    }

    render() {
        return <InputGroup className="mb-3">
            <Link to='#' type="text" className="form-control" onClick={(e: any) => this.onEdit(e)}>{this.state.text}</Link>
            <div className="input-group-append">
                <button className="btn btn-dark" type="button" onClick={e => this.onEdit(e)}><MdEdit/></button>
                <button className="btn btn-danger" type="button" onClick={e => this.onDelete(e)}><MdDelete/></button>
            </div>
        </InputGroup>;
    }

    private onEdit(e: FormEvent<HTMLButtonElement>) {
        e.preventDefault();

        this.props.onEdit(this.state.id);
    }

    private onDelete(e: FormEvent<HTMLButtonElement>) {
        e.preventDefault();
        axios.delete(buildDeleteAnswerUrl(this.state.id))
             .then(({ status }) => {
                 if (status !== 200) {
                     throw new Error('Cannot delete the answer');
                 }
                 this.props.onDelete(this.state.id);
             });
    }
}