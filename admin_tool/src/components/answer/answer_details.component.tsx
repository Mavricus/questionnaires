import React, { FunctionComponent } from 'react';
import { Form, FormGroup } from 'react-bootstrap';

interface Props {
    id: string;
    text: string;
    onTextChange: (text: string) => void;
}

export const AnswerDetails: FunctionComponent<Props> = (props: Props)  => {
    return <FormGroup>
            <Form.Label>Description</Form.Label>
        <Form.Control
            id={`description-${props.id}`}
            required={true}
            type="text"
            placeholder="Answer text"
            defaultValue={props.text}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => props.onTextChange(e.target.value)}/>
    </FormGroup>
};