import React, { Component, } from 'react';
import { Form, Navbar } from 'react-bootstrap';
import { MdBuild } from 'react-icons/md';
import { Link } from 'react-router-dom';

interface Props {
}

interface State {
}

export class NavigationBar extends Component<Props, State> {
    render() {
        return <Navbar bg="light">
            <Navbar.Brand href="/">Questionnaires Administration Tool</Navbar.Brand>
            <Navbar.Toggle/>
            <Navbar.Collapse className="justify-content-end">
                <Form inline>
                    <Link to="/configuration" className="text-dark"><MdBuild/></Link>
                </Form>
            </Navbar.Collapse>
        </Navbar>
    }
}